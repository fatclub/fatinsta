var error_url = 0;
var attrs;
var pages_order = [];
var hover = 0;
var wait = 0;
$(document).ready(function () {
//    $(".sortable tbody").sortable({
//        
//    });
//var yolo = ($( ".selector" ).sortable( "option", "items" ));
//console.info(yolo); 
//$( ".sortable tbody" ).sortable();
//var sortedIDs = $( ".sortable tbody").sortable( "toArray" );
    $(".sideClick").click(function () {
        if ($(".funkce").css("display") === "block") {
            $(".funkce").fadeOut(300)
            $(".sideClick").html(">>");
        } else {
            $(".funkce").fadeIn(300);
            $(".sideClick").html("<<");
        }
    });
    $(".sortable tbody").sortable({
        update: function () {
            var arr = $(this).sortable('toArray');
            var i, n;
            attrs = [];
            for (i = 0, n = arr.length; i < n; i++) {
                attrs.push($('#' + arr[i]).attr('data-myattr'));
            }
            console.info(attrs);
        }
    });
    $(".clickable_ul").sortable({
        update: function () {
            console.info(":-(");
            var arr = $(this).sortable('toArray');
            var yolo = [];

            console.info(arr);
            for (n = 0; n < arr.length; n++) {
                yolo.push($("#" + arr[n] + " ul").sortable("toArray"));
            }
            console.info(yolo);
            console.info($(this));
            var i, n;
            attrs = [];
            for (i = 0, n = arr.length; i < n; i++) {
                attrs.push($('#' + arr[i]).attr('data-id'));
            }
            console.info(attrs);
        }
    });
    $(".clickable_ul li").on("mouseover", function () {
        if (hover !== 1) {
            $(this).parents().find(".admin_list_pages_edit").hide();
            $("#" + $(this).attr("id") + ".admin_list_pages_edit").show();
            hover = 1;
        }
    });
    $(".deletePage").on("click", function () {
        console.info(window.location);
        str = window.location.search;
        index = window.location.search.lastIndexOf("=");
        addr = "";
        for (i = index + 1; i < str.length; i++) {
            addr += str[i];
        }
        window.location.search = "?action=deletePage&id=" + addr;
    });
    $(".clickable_ul li").on("mouseleave", function () {
        $(".admin_list_pages_edit").hide();
        hover = 0;
    });
    $(".sub_ul_admin").sortable({
        update: function () {
            console.info(":(");
            var arr = $(this).sortable('toArray');
            console.info($(this).attr("data-id"));
            var i, n;
            attrs = [];
            for (i = 0, n = arr.length; i < n; i++) {
                attrs.push($('#' + arr[i]).attr('data-id'));
            }
            console.info(attrs);
        }
    });
    $("#saveHeaderOrder").click(function () {
        saveHeaderOrder();
    });
    $("#saveUpdatesOrder").click(function () {
        saveUpdatesOrder();
    });
    $(".sortable tbody").disableSelection();
    $(".clickable_ul").disableSelection();
    $(".clickable tbody tr").on("click", function () {
        var click = $(this).contents()[0].innerHTML;
        address = window.location.href.split("/");
        newaddress = "";
        for (i = 0; i < address.length - 1; i++) {
            newaddress += address[i] + "/";
        }
        console.info(newaddress);
        window.location.href = newaddress + "?" + action + "&id=" + click;
    });
    $(".admin_list_pages_edit").on("click", function () {
        console.info("click");
        var click = $(this).attr("data-id");
        address = window.location.href.split("/");
        newaddress = "";
        for (i = 0; i < address.length - 1; i++) {
            newaddress += address[i] + "/";
        }
        console.info(newaddress);
        window.location.href = newaddress + "?" + action + "&id=" + click;
    });
    $(".clickable_ul a").on("click", function () {

    });
    if ($("#nadstranka").length === 1) {
        $("input[name='parent']").change(function () {
            checkparent();
        });
        checkparent();
    }
    if ($("#url").length === 1) {
        $("input[name='address']").change(function () {
            checkurl();
        });
    }
    function saveHeaderOrder() {
        $.post("ajax_saveHeaderOrder", {"pole": attrs}, function (r) {
            console.info(r);
            if (isJSON(r) === true) {
                alert("Saving successful");
            } else {
                alert("Něco se pokazilo, pravděpodobně se vše však povedlo.");
            }
        });
    }
    function saveUpdatesOrder() {
        $.post("ajax_saveUpdatesOrder", {"pole": attrs}, function (r) {
            console.info(r);
            if (isJSON(r) === true) {
                alert("Saving successful");
            } else {
                alert("Něco se pokazilo, pravděpodobně se vše však povedlo.");
            }
        });
    }
    function isJSON(data) {
        try {
            JSON.parse(data);
        } catch (e) {
            return e;
        }
        return true;
    }
    function checkparent() {
        $.get("ajax_parent", {"a": parseInt($("input[name='parent']").val())}, function (e) {
            $("#nadstranka").html(e);
        });
    }
    function checkurl() {
        wait = 1;
        if ($("input[name='address']").val() !== $("input[name='address']").attr("value")) {
            $("input[name='address']").val(checkurltext($("input[name='address']").val()));
//            console.info($("input[name='address']").val());
            $.get("ajax_url", {"a": ($("input[name='address']").val())}, function (e) {
                console.info($("input[name='address']").val());
                console.info(e);
                data = $.parseJSON(e);
                error_url = data.error;
                $("#url").html(data.msg);
                wait = 0;
            });
        }
    }
    function checkurltext(addr) {
        if (addr[addr.length - 1] !== "/") {
            addr = addr + "/";
        }
        if (addr[0] !== "/") {
            addr = "/" + addr;
        }
        //check for illeagal chars
        return addr;
    }
    $("#page_form").submit(function (e) {
//        e.preventDefault();
        if (error_url === 1) {

            alert("Opravte chyby");
            return false;
        }
        if (wait === 1) {
            alert("Vyčkejte na zkontrolování");
            return false;
        }
    });
});

