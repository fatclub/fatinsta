<footer>
    <div class="footer_nav">
        <img href="https://www.facebook.com/" class="footer-photo" src="../img/FB-f-Logo__blue_1024.png" alt=""/>
        <img href="https://www.instagram.com/" class="footer-photo" src="../img/In-2CRev-128px-TM.png" alt=""/>
        <img href="https://twitter.com" class="footer-photo" src="../img/TwitterLogo_55acee.png" alt=""/>
        <img href="https://www.youtube.com/" class="footer-photo" src="../img/YouTube-icon-full_color.png" alt=""/>
        <img href="https://plus.google.com/" class="footer-photo" src="../img/btn_google+_dark_normal_ios@3x.png" alt=""/>
    </div>
</footer>