<?php class Page{
public $id;
public $title;
public $html;
public $address;
public $specialPage;
public $description;

public function __construct() {
    DBM::pripoj();
    $address = $_SERVER["REQUEST_URI"];
    if(!empty($_GET)){
        echo strlen($address)-strpos($address,"?")."<br>".strpos($address,"?")."<br>".strlen($address);
        $address=substr($address,0,strpos($address,"?"));
        
    }
    $pole = DBM::dotaz("SELECT * FROM pages WHERE url LIKE ?",array($address));
//    var_dump($pole);
    if(empty($pole)){
        require './404page.php';
        exit();
    }
    $this->id=$pole[0]["id"];
    $this->title=$pole[0]["title"];
    $this->showSideMenu=$pole[0]["showSideMenu"];
    $this->html=$pole[0]["html"];
    $this->address=$pole[0]["url"];
    $this->specialPage=$pole[0]["specialPage"];
    $this->showSideButton=$pole[0]["showSideButton"];
    $this->description=$pole[0]["description"];
//    var_dump($this);
//    var_dump($pole);
    
}
public function footer(){
    require 'getFooter.php';
}
public function header(){
    require 'obalHeader.php';
}
public function head(){
    require 'getHead.php';
    
}
public function mainContent(){
//    var_dump($this);
    if($this->specialPage == 1){
        require 'mainExtraContent.php';
    }else{
        require 'mainContent.php';
    }
}
public function sideButton(){
    if($this->showSideButton==1){
        require 'sideButton.php';
    }
}
}
