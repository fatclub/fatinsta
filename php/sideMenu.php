<?php

try {
    $parent = 0;
    echo '<section class="side_menu">
    <ul class="side_menu_list">';

    function display_children($parent, $level) {
        $sideMenu = DBM::dotaz("SELECT pages.id, pages.title, pages.address, Deriv1.Count FROM pages  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM pages GROUP BY parent) Deriv1 ON pages.id = Deriv1.parent WHERE pages.parent=? ORDER BY poradi", array($parent));
        foreach ($sideMenu as $value) {
            if ($value["Count"] > 0) {
                echo ' <li class="' . Url::getSideClass($value, $level) . '"><span class="' . Url::getSpanClass($level) . ' sub_menu_down"><i class="fa fa-chevron-down"></i></span><a href="' . $value["address"] . '">' . $value["title"] . '</a>'
                . '<ul class="side_menu_sublist">';
                display_children($value["id"], $level + 1);
                echo '</ul></li>';
            } else {
                //class="'.getSideClass($value,$level).'"
                echo '<li class="' . Url::getSideClass($value, $level) . '">
            <a href="' . $value["address"] . '">' . $value["title"] . '</a>';
                echo '</li>';
            }
        }
    }

    display_children($parent, 0);

    echo '</ul>
</section>';
} catch (Exception $ex) {
    
}
?>
