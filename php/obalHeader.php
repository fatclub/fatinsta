<header>
<img class="burt" src="/img/logo.jpg"/>
    <div class="header_nav">
        <div class="nadpis n-tittle">
            <h3>B.U.Ř.T. - "Burzovní univerzální řízené transakce"</h3> 
        </div>
        <ul>
            <?php
            $header = DBM::dotaz("SELECT pages.title, pages.url FROM header JOIN pages ON (header.page_id = pages.id) ORDER BY position");
            foreach ($header as $value):
                ?>
                <li class="tittle"><a href="<?= $value["url"]; ?>"><?= $value["title"]; ?></a></li>
<?php endforeach; ?>
        </ul>
    </div>
</header>