<html lang="en-US"><head>

<title>HTML Color Picker</title>
<!--<script async="" src="//cse.google.com/adsense/search/async-ads.js"></script>-->
<!--<script type="text/javascript" async="" src="http://www.google.com/cse/cse.js?cx=012971019331610648934:m2tou3_miwy"></script>-->
<!--<script async="" type="text/javascript" src="http://www.googletagservices.com/tag/js/gpt.js"></script>-->
<!--<script async="" src="//www.google-analytics.com/analytics.js"></script>-->
<script src="w3/tinycolor.js"></script><style type="text/css"></style>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<meta name="Keywords" content="HTML,CSS,JavaScript,SQL,PHP,jQuery,ASP,XML,DOM,Bootstrap,Web development,W3C,tutorials,programming,training,learning,quiz,primer,lessons,references,examples,source code,colors,demos,tips">
<meta name="Description" content="Well organized and easy to understand Web bulding tutorials with lots of examples of how to use HTML, CSS, JavaScript, SQL, PHP, and XML.">-->
<!--<link rel="icon" href="/favicon.ico" type="image/x-icon">-->
<link rel="stylesheet" href="w3/w3.css">

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-3855518-1', 'auto');
ga('require', 'displayfeatures');
ga('send', 'pageview');
</script>

<script type="text/javascript">
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type="text/javascript">
// GPT slots
var gptAdSlots = [];
googletag.cmd.push(function() {
var leaderMapping = googletag.sizeMapping().
// Mobile ad
addSize([0, 0], [320, 50]). 
// Vertical Tablet ad
addSize([480, 0], [468, 60]). 
// Horizontal Tablet
addSize([700, 0], [728, 90]).
// Desktop and bigger ad
addSize([1200, 0], [728, 90]).build();   
gptAdSlots[0] = googletag.defineSlot('/16833175/MainLeaderboard', [728, 90], 'div-gpt-ad-1422003450156-2').   
defineSizeMapping(leaderMapping).addService(googletag.pubads());
var skyMapping = googletag.sizeMapping().
// Mobile ad
addSize([0, 0], [320, 50]). 
// Tablet ad
addSize([975, 0], [120, 600]). 
// Desktop
addSize([1100, 0], [160, 600]).   
// Large Desktop
addSize([1675, 0], [[160, 600], [300, 600]]).build();   
gptAdSlots[1] = googletag.defineSlot('/16833175/WideSkyScraper', [[160, 600], [300, 600]], 'div-gpt-ad-1422003450156-5').
defineSizeMapping(skyMapping).addService(googletag.pubads());
var bmrMapping = googletag.sizeMapping().
// Smaller
addSize([0, 0], [[300, 250], [336, 280]]). 
// Large Desktop
addSize([1200, 0], [[300, 250], [336, 280], [970, 250]]).build();
gptAdSlots[2] = googletag.defineSlot('/16833175/BottomMediumRectangle', [[300, 250], [336, 280], [970, 250]], 'div-gpt-ad-1422003450156-0').
defineSizeMapping(bmrMapping).setCollapseEmptyDiv(true).addService(googletag.pubads());
gptAdSlots[3] = googletag.defineSlot('/16833175/RightBottomMediumRectangle', [[300, 250], [336, 280]], 'div-gpt-ad-1422003450156-3').addService(googletag.pubads());
googletag.pubads().setTargeting("content","tags");
googletag.enableServices();
});
</script>
<link rel="stylesheet" href="w3/w3.css">
<link rel="stylesheet" type="text/css" href="w3/stdtheme.css">
<style>
#selectedcolor {
	border:1px solid #e3e3e3;
	width:80%;
	height:300px;
	margin:auto;
}
#divpreview {
	border:1px solid #e3e3e3;
	width:80px;
	height:20px;
	margin:auto;
	visibility:hidden;
}
.colorTable, #colorhexDIV, #colorrgbDIV, #colorhsvDIV, #colorhslDIV, #colornamDIV {
	font-family:Consolas, 'Courier New', Courier, monospace;
}
#colorhexDIV, #colorrgbDIV, #colorhslDIV, #colorhsvDIV, #colornamDIV {
	font-size:18px;
}
#wronginputDIV {
    text-align:left;
    position:absolute;
    margin:4px 10px;
    color:#a94442;
    display:none;
}
.has-error input{
    border:1px solid red;
}
#entercolorDIV input,#entercolorDIV button{
	height:28px;
}
#entercolorDIV input{
	width:80%;
	border:1px solid #d3d3d3;
	border-right:none;
}

.w3-table-all td {
	padding:4px;
}
</style>
<script>
<!--
var colorhex = "FF0000";
function mouseOverColor(hex) {
    document.getElementById("divpreview").style.visibility = "visible";
    document.getElementById("divpreview").style.backgroundColor = hex;
    document.body.style.cursor = "pointer";
}
function mouseOutMap() {
    if (hh == 0) {
        document.getElementById("divpreview").style.visibility = "hidden";
    } else {
      hh = 0;
    }
    document.getElementById("divpreview").style.backgroundColor = "#" + colorhex;
    document.body.style.cursor = "";
}
var hh = 0;
function clickColor(hex, seltop, selleft, html5) {
    var c;
    if (html5 && html5 == 5)	{
        c = document.getElementById("html5colorpicker").value;
    } else {
        if (hex == 0)	{
            c = document.getElementById("entercolor").value;
        } else {
            c = hex;
        }
    }
    colorhex = tinycolor(c).toHexString();
    if (tinycolor(c).isValid()) {
        clearWrongInput();
    } else {
        wrongInput();
        return;
    }
    r = tinycolor(c).toRgb().r;
    g = tinycolor(c).toRgb().g;
    b = tinycolor(c).toRgb().b;
    document.getElementById("colornamDIV").innerHTML = (tinycolor(colorhex).toName() || "");
    document.getElementById("colorhexDIV").innerHTML = tinycolor(colorhex).toHexString();
    document.getElementById("colorrgbDIV").innerHTML = tinycolor(colorhex).toRgbString();
    document.getElementById("colorhslDIV").innerHTML = tinycolor(colorhex).toHslString();    
    document.getElementById("colorhsvDIV").innerHTML = tinycolor(colorhex).toHsvString();        
    if ((seltop+200)>-1 && selleft>-1) {
        document.getElementById("selectedhexagon").style.top=seltop + "px";
        document.getElementById("selectedhexagon").style.left=selleft + "px";
        document.getElementById("selectedhexagon").style.visibility="visible";
	} else {
        document.getElementById("divpreview").style.backgroundColor = tinycolor(colorhex).toHexString();
        document.getElementById("selectedhexagon").style.visibility = "hidden";
	}
    document.getElementById("selectedcolor").style.backgroundColor = tinycolor(colorhex).toHexString();
    document.getElementById("html5colorpicker").value = tinycolor(colorhex).toHexString();  
  document.getElementById('slideRed').value = r;
  document.getElementById('slideGreen').value = g;
  document.getElementById('slideBlue').value = b;
  changeRed(r);changeGreen(g);changeBlue(b);
  changeColor();
  document.getElementById("fixed").style.backgroundColor = tinycolor(colorhex).toHexString();
}
function wrongInput() {
    document.getElementById("entercolorDIV").className = "has-error";
    document.getElementById("wronginputDIV").style.display = "block";
}
function clearWrongInput() {
    document.getElementById("entercolorDIV").className = "";
    document.getElementById("wronginputDIV").style.display = "none";
}
function changeRed(value) {
    document.getElementById('valRed').innerHTML = value;
    changeAll();
}
function changeGreen(value) {
    document.getElementById('valGreen').innerHTML = value;
    changeAll();
}
function changeBlue(value) {
    document.getElementById('valBlue').innerHTML = value;
    changeAll();
}
function changeAll() {
    var r = document.getElementById('valRed').innerHTML;
    var g = document.getElementById('valGreen').innerHTML;
    var b = document.getElementById('valBlue').innerHTML;
    document.getElementById('change').style.backgroundColor = "rgb(" + r + "," + g + "," + b + ")";
    document.getElementById('changetxt').innerHTML = "rgb(" + r + ", " + g + ", " + b + ")";
    document.getElementById('changetxthex').innerHTML = tinycolor("rgb(" + r + "," + g + "," + b + ")").toHexString();
}

function hslLum_top() {
  var i, a, match;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsl(" + h + "," + s + "," + (i * 0.05) + ")"));
  }
  arr.reverse();
  a = "<h3 class='w3-center'>Lighter / Darker:</h3><table class='colorTable' style='width:100%;'>";
  //a += "<tr>";
  //a += "<td style='width:40px;'></td>";
  //a += "<td style='text-align:center;'><h3>Lighter / Darker</h3></td>";
  //a += "<td style='width:80px;'></td>";
  //a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(l * 100) == Math.round(tinycolor(arr[i]).toHsl().l * 100)) {
      a += "<tr><td></td><td></td><td></td></tr>";
      a += "<tr>";
      a += "<td style='text-align:right;'><b>" + Math.round(l * 100) + "%&nbsp;</b></td>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'><br><br></td>";
      a += "<td>&nbsp;<b>" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "</tr>";
      a += "<tr><td></td><td></td><td></td></tr>";
      match = 1;      
    } else {
      if (match == 0 && l > tinycolor(arr[i]).toHsl().l) {
        a += "<tr><td></td><td></td><td></td></tr>";
        a += "<tr>";
        a += "<td style='text-align:right;'><b>" + Math.round(l * 100) + "%&nbsp;</b></td>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td>&nbsp;<b>" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "</tr>";
        a += "<tr><td></td><td></td><td></td></tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='width:40px;text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsl().l * 100) + "%&nbsp;</td>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='width:80px;'>&nbsp;" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "</tr>";
    }
  }
  a += "</table>";
  document.getElementById("lumtopcontainer").innerHTML = a;
}

function hslHue_top() {
  var i, a, match, loopH;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 12; i++) {
      arr.push(tinycolor("hsl(" + (i * 30) + "," + s + "," + l + ")"));
  }
  a = "<table class='colorTable' style='width:100%;line-height:1.95;'>";
  a += "<tr>";
  a += "<td style='width:40px;'></td>";
  a += "<td style='text-align:center;line-height:1.5'><h3>Hue:</h3></td>";
  a += "<td style='width:80px;'></td>";
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    loopH = Math.round(tinycolor(arr[i]).toHsl().h)
    if (i == arr.length - 1) {loopH = 360;}
    if (match == 0 && Math.round(h) == loopH) {
      a += "<tr><td></td><td></td></tr>";
      a += "<tr>";
      a += "<td style='width:40px;text-align:right;'><b>" + Math.round(h) + "&nbsp;</b></td>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'><br><br></td>";
      a += "<td style='width:80px;'><b>&nbsp;" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "</tr>";
      a += "<tr><td></td><td></td></tr>";
      match = 1;      
    } else {
      if (match == 0 && Math.round(h) < loopH) {
        a += "<tr><td></td><td></td></tr>";
        a += "<tr>";
        a += "<td style='width:40px;text-align:right;'><b>" + Math.round(h) + "&nbsp;</b></td>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td style='width:80px;'><b>&nbsp;" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "</tr>";
        a += "<tr><td></td><td></td></tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='width:40px;text-align:right;'>" + loopH + "&nbsp;</td>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='width:80px;'>&nbsp;" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "</tr>";
    }
  }
  a += "</table>";
  document.getElementById("huetopcontainer").innerHTML = a;
}

function hslHue() {
  var i, a, match, loopH;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 24; i++) {
      arr.push(tinycolor("hsl(" + (i * 15) + "," + s + "," + l + ")"));
  }
  a = "<h3>Hue</h3>";
  a += "<div class='table-responsive'>";
  a += "<table class='w3-table-all colorTable' style='width:100%;white-space: nowrap;font-size:14px;'>";
  a += "<tr>";
  a += "<td style='width:150px;'></td>";
  a += "<td style='text-align:right;'>Hue&nbsp;</td>";
  a += "<td>Hex</td>";
  a += "<td>Rgb</td>";
  a += "<td>Hsl</td>";
  a += "<td>Hsv</td>";  
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    loopH = Math.round(tinycolor(arr[i]).toHsl().h)
    if (i == arr.length - 1) {loopH = 360;}
    if (match == 0 && Math.round(h) == loopH) {
      a += "<tr>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
      a += "<td style='text-align:right;'><b>" + Math.round(h) + "&nbsp;</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toRgbString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHslString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHsvString() + "</b></td>";
      a += "</tr>";
      match = 1;      
    } else {
      if (match == 0 && Math.round(h) < loopH) {
        a += "<tr>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td style='text-align:right;'><b>" + Math.round(h) + "&nbsp;</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toRgbString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHslString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHsvString() + "&nbsp;</b></td>";
        a += "</tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='text-align:right;'>" + loopH + "&nbsp;</td>";
      a += "<td>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toRgbString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHslString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHsvString() + "</td>";                  
      a += "</tr>";
    }
  }
  a += "</table></div>";
  document.getElementById("huecontainer").innerHTML = a;
}

function hslSat() {
  var i, a, match, loopH;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsl(" + h + "," + (i * 0.05) + "," + l + ")"));
  }
  arr.reverse();
  a = "<h3>HSL Saturation</h3>";
  a += "<div class='table-responsive'>";
  a += "<table class='w3-table-all colorTable' style='width:100%;white-space: nowrap;font-size:14px;'>";
  a += "<tr>";
  a += "<td style='width:150px;'></td>";
  a += "<td style='text-align:right;'>Sat&nbsp;</td>";
  a += "<td>Hex</td>";
  a += "<td>Rgb</td>";
  a += "<td>Hsl</td>";
  a += "<td>Hsv</td>";  
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(s * 100) == Math.round(tinycolor(arr[i]).toHsl().s * 100)) {
      a += "<tr>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
      a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hslObj).toHsl().s *100) + "%&nbsp;</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toRgbString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHslString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHsvString() + "</b></td>";
      a += "</tr>";
      match = 1;      
    } else {
      if (match == 0 && s > tinycolor(arr[i]).toHsl().s) {
        a += "<tr>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hslObj).toHsl().s *100) + "%&nbsp;</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toRgbString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHslString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHsvString() + "&nbsp;</b></td>";
        a += "</tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsl().s *100) + "%&nbsp;</td>";
      a += "<td>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toRgbString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHslString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHsvString() + "</td>";                  
      a += "</tr>";
    }
  }
  a += "</table></div>";
  document.getElementById("hslsatcontainer").innerHTML = a;
}

function hslLum() {
  var i, a, match, loopH;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsl(" + h + "," + s + "," + (i * 0.05) + ")"));
  }
  arr.reverse();
  a = "<h3>HSL Lighter / Darker</h3>";
  a += "<div class='table-responsive'>";
  a += "<table class='w3-table-all colorTable' style='width:100%;white-space: nowrap;font-size:14px;'>";
  a += "<tr>";
  a += "<td style='width:150px;'></td>";
  a += "<td style='text-align:right;'>Lum&nbsp;</td>";
  a += "<td>Hex</td>";
  a += "<td>Rgb</td>";
  a += "<td>Hsl</td>";
  a += "<td>Hsv</td>";  
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(l * 100) == Math.round(tinycolor(arr[i]).toHsl().l * 100)) {
      a += "<tr>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
      a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hslObj).toHsl().l *100) + "%&nbsp;</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toRgbString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHslString() + "</b></td>";
      a += "<td><b>" + tinycolor(hslObj).toHsvString() + "</b></td>";
      a += "</tr>";
      match = 1;      
    } else {
      if (match == 0 && l > tinycolor(arr[i]).toHsl().l) {
        a += "<tr>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hslObj).toHsl().l * 100) + "%&nbsp;</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toRgbString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHslString() + "</b></td>";
        a += "<td><b>" + tinycolor(hslObj).toHsvString() + "&nbsp;</b></td>";
        a += "</tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsl().l *100) + "%&nbsp;</td>";
      a += "<td>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toRgbString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHslString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHsvString() + "</td>";                  
      a += "</tr>";
    }
  }
  a += "</table></div>";
  document.getElementById("hsllumcontainer").innerHTML = a;
}

function hsvSat() {
  var i, a, match, loopH;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hsvObj = tinycolor(color).toHsv();
  var h = hsvObj.h;
  var s = hsvObj.s;
  var v = hsvObj.v;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsv(" + h + "," + (i * 0.05) + "," + v + ")"));
  }
  arr.reverse();
  a = "<h3>HSV Saturation</h3>";
  a += "<div class='table-responsive'>";
  a += "<table class='w3-table-all colorTable' style='width:100%;white-space: nowrap;font-size:14px;'>";
  a += "<tr>";
  a += "<td style='width:150px;'></td>";
  a += "<td style='text-align:right;'>Sat&nbsp;</td>";
  a += "<td>Hex</td>";
  a += "<td>Rgb</td>";
  a += "<td>Hsl</td>";
  a += "<td>Hsv</td>";  
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(s * 100) == Math.round(tinycolor(arr[i]).toHsv().s * 100)) {
      a += "<tr>";
      a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'></td>";
      a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hsvObj).toHsv().s * 100) + "%&nbsp;</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toRgbString() + "</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHslString() + "</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHsvString() + "</b></td>";
      a += "</tr>";
      match = 1;      
    } else {
      if (match == 0 && s > tinycolor(arr[i]).toHsv().s) {
        a += "<tr>";
        a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'></td>";
        a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hsvObj).toHsv().s * 100) + "%&nbsp;</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toRgbString() + "</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHslString() + "</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHsvString() + "&nbsp;</b></td>";
        a += "</tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsv().s * 100) + "%&nbsp;</td>";
      a += "<td>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toRgbString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHslString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHsvString() + "</td>";                  
      a += "</tr>";
    }
  }
  a += "</table></div>";
  document.getElementById("hsvsatcontainer").innerHTML = a;
}

function hsvVal() {
  var i, a, match, loopH;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hsvObj = tinycolor(color).toHsv();
  var h = hsvObj.h;
  var s = hsvObj.s;
  var v = hsvObj.v;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsv(" + h + "," + s + "," + (i * 0.05) + ")"));
  }
  arr.reverse();
  a = "<h3>HSV Brighter / Darker</h3>";
  a += "<div class='table-responsive'>";
  a += "<table class='w3-table-all colorTable' style='width:100%;white-space: nowrap;font-size:14px;'>";
  a += "<tr>";
  a += "<td style='width:150px;'></td>";
  a += "<td style='text-align:right;'>Value&nbsp;</td>";
  a += "<td>Hex</td>";
  a += "<td>Rgb</td>";
  a += "<td>Hsl</td>";
  a += "<td>Hsv</td>";  
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(v * 100) == Math.round(tinycolor(arr[i]).toHsv().v * 100)) {
      a += "<tr>";
      a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'></td>";
      a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hsvObj).toHsv().v * 100) + "%&nbsp;</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toRgbString() + "</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHslString() + "</b></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHsvString() + "</b></td>";
      a += "</tr>";
      match = 1;      
    } else {
      if (match == 0 && v > tinycolor(arr[i]).toHsv().v) {
        a += "<tr>";
        a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'></td>";
        a += "<td style='text-align:right;'><b>" + Math.round(tinycolor(hsvObj).toHsv().v * 100) + "%&nbsp;</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toRgbString() + "</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHslString() + "</b></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHsvString() + "&nbsp;</b></td>";
        a += "</tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsv().v * 100) + "%&nbsp;</td>";
      a += "<td>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toRgbString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHslString() + "</td>";
      a += "<td>" + tinycolor(arr[i]).toHsvString() + "</td>";                  
      a += "</tr>";
    }
  }
  a += "</table></div>";
  document.getElementById("hsvvalcontainer").innerHTML = a;
}

function hsvBrighten() {
  var i, a, match;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hsvObj = tinycolor(color).toHsv();
  var h = hsvObj.h;
  var s = hsvObj.s;
  var v = hsvObj.v;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsv(" + h + "," + s + "," + (i * 0.05) + ")"));
  }
  arr.reverse();
  a = "<table class='colorTable' style='width:100%;'>";
  a += "<tr>";
  a += "<td style='width:40px;'></td>";
  a += "<td style='text-align:center;'><h3>HSV<br>Brighten / Darken</h3></td>";
  a += "<td style='width:80px;'></td>";
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(v * 100) == Math.round(tinycolor(arr[i]).toHsv().v * 100)) {
      a += "<tr><td></td><td></td><td></td></tr>";
      a += "<tr>";
      a += "<td style='text-align:right;'><b>" + Math.round(v * 100) + "%&nbsp;</b></td>";
      a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'><br><br></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
      a += "</tr>";
      a += "<tr><td></td><td></td><td></td></tr>";
      match = 1;      
    } else {
      if (match == 0 && v > tinycolor(arr[i]).toHsv().v) {
        a += "<tr><td></td><td></td><td></td></tr>";
        a += "<tr>";
        a += "<td style='text-align:right;'><b>" + Math.round(v * 100) + "%&nbsp;</b></td>";
        a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'></td>";
        a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
        a += "</tr>";
        a += "<tr><td></td><td></td><td></td></tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='width:40px;text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsv().v * 100) + "%&nbsp;</td>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='width:80px;'>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "</tr>";
    }
  }
  a += "</table>";
  document.getElementById("hsvbrightencontainer").innerHTML = a;
}

function hsvSaturation() {
  var i, a, match;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hsvObj = tinycolor(color).toHsv();
  var h = hsvObj.h;
  var s = hsvObj.s;
  var v = hsvObj.v;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsv(" + h + "," + (i * 0.05) + "," + v + ")"));
  }
  arr.reverse();
  a = "<table class='colorTable' style='width:100%;'>";
  a += "<tr>";
  a += "<td style='width:40px;'></td>";
  a += "<td style='text-align:center;'><h3>HSV<br>Saturation</h3></td>";
  a += "<td style='width:80px;'></td>";
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(s * 100) == Math.round(tinycolor(arr[i]).toHsv().s * 100)) {
      a += "<tr><td></td><td></td></tr>";
      a += "<tr>";
      a += "<td style='text-align:right;'><b>" + Math.round(s * 100) + "%&nbsp;</b></td>";
      a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'><br><br></td>";
      a += "<td><b>" + tinycolor(hsvObj).toHexString() + "</b></td>";
      a += "</tr>";
      a += "<tr><td></td><td></td></tr>";
      match = 1;      
    } else {
      if (match == 0 && s > tinycolor(arr[i]).toHsv().s) {
        a += "<tr><td></td><td></td></tr>";
        a += "<tr>";
        a += "<td style='text-align:right;'><b>" + Math.round(s * 100) + "%&nbsp;</b></td>";
        a += "<td style='background-color:" + tinycolor(hsvObj).toHexString() + "'></td>";
        a += "<td><b>&nbsp;" + tinycolor(hsvObj).toHexString() + "</b></td>";
        a += "</tr>";
        a += "<tr><td></td><td></td></tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsv().s * 100) + "%&nbsp;</td>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td>&nbsp;" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "</tr>";
    }
  }
  a += "</table>";
  document.getElementById("hsvsaturationcontainer").innerHTML = a;
}

function hslSaturation() {
  var i, a, match;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsl(" + h + "," + (i * 0.05) + "," + l + ")"));
  }
  arr.reverse();
  a = "<table class='colorTable' style='width:100%;'>";
  a += "<tr>";
  a += "<td style='width:40px;'></td>";
  a += "<td style='text-align:center;'><h3>HSL<br>Saturation</h3></td>";
  a += "<td style='width:80px;'></td>";
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(s * 100) == Math.round(tinycolor(arr[i]).toHsl().s * 100)) {
      a += "<tr><td></td><td></td></tr>";
      a += "<tr>";
      a += "<td style='text-align:right;'><b>" + Math.round(s * 100) + "%&nbsp;</b></td>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'><br><br></td>";
      a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "</tr>";
      a += "<tr><td></td><td></td></tr>";
      match = 1;      
    } else {
      if (match == 0 && s > tinycolor(arr[i]).toHsl().s) {
        a += "<tr><td></td><td></td></tr>";
        a += "<tr>";
        a += "<td style='text-align:right;'><b>" + Math.round(s * 100) + "%&nbsp;</b></td>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td><b>&nbsp;" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "</tr>";
        a += "<tr><td></td><td></td></tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsl().s * 100) + "%&nbsp;</td>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td>&nbsp;" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "</tr>";
    }
  }
  a += "</table>";
  document.getElementById("saturationcontainer").innerHTML = a;
}

function hslLumen() {
  var i, a, match;
  var color = document.getElementById("colorhexDIV").innerHTML;
  var hslObj = tinycolor(color).toHsl();
  var h = hslObj.h;
  var s = hslObj.s;
  var l = hslObj.l;
  var arr = [];
  for (i = 0; i <= 20; i++) {
      arr.push(tinycolor("hsl(" + h + "," + s + "," + (i * 0.05) + ")"));
  }
  arr.reverse();
  a = "<table class='colorTable' style='width:100%;'>";
  a += "<tr>";
  a += "<td style='width:40px;'></td>";
  a += "<td style='text-align:center;'><h3>HSL<br>Lighter / Darker</h3></td>";
  a += "<td style='width:80px;'></td>";
  a += "</tr>";  
  match = 0;
  for (i = 0; i < arr.length; i++) {
    if (match == 0 && Math.round(l * 100) == Math.round(tinycolor(arr[i]).toHsl().l * 100)) {
      a += "<tr><td></td><td></td><td></td></tr>";
      a += "<tr>";
      a += "<td style='text-align:right;'><b>" + Math.round(l * 100) + "%&nbsp;</b></td>";
      a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'><br><br></td>";
      a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
      a += "</tr>";
      a += "<tr><td></td><td></td><td></td></tr>";
      match = 1;      
    } else {
      if (match == 0 && l > tinycolor(arr[i]).toHsl().l) {
        a += "<tr><td></td><td></td><td></td></tr>";
        a += "<tr>";
        a += "<td style='text-align:right;'><b>" + Math.round(l * 100) + "%&nbsp;</b></td>";
        a += "<td style='background-color:" + tinycolor(hslObj).toHexString() + "'></td>";
        a += "<td><b>" + tinycolor(hslObj).toHexString() + "</b></td>";
        a += "</tr>";
        a += "<tr><td></td><td></td><td></td></tr>";
        match = 1;
      }
      a += "<tr>";
      a += "<td style='width:40px;text-align:right;'>" + Math.round(tinycolor(arr[i]).toHsl().l * 100) + "%&nbsp;</td>";
      a += "<td style='cursor:pointer;background-color:" + tinycolor(arr[i]).toHexString() + "' onclick='clickColor(\"" + tinycolor(arr[i]).toHexString() + "\")'></td>";
      a += "<td style='width:80px;'>" + tinycolor(arr[i]).toHexString() + "</td>";
      a += "</tr>";
    }
  }
  a += "</table>";
  document.getElementById("lumencontainer").innerHTML = a;
}

function changeColor(value) {
//hslHue_top();
  hslLum_top();  
  hslHue();  
  hslSat();
  hslLum();
  hsvSat();
  hsvVal();  
  //hslSaturation();  
  //hslLumen();
  //hsvSaturation();
  //hsvBrighten();  
}

window.onload = function() {
    var x = document.createElement("input");
    x.setAttribute("type", "color");
    if (x.type == "text") {
        document.getElementById("html5DIV").style.visibility = "hidden";
    }
} 
//-->
</script>
<script src="http://partner.googleadservices.com/gpt/pubads_impl_78.js" async=""></script><script src="https://www.google.com/jsapi?autoload=%7B%22modules%22%3A%5B%7B%22name%22%3A%22search%22%2C%22version%22%3A%221.0%22%2C%22callback%22%3A%22__gcse.scb%22%2C%22style%22%3A%22https%3A%2F%2Fwww.google.com%2Fcse%2Fstyle%2Flook%2Fv2%2Fdefault.css%22%2C%22language%22%3A%22en%22%7D%5D%7D" type="text/javascript"></script><link type="text/css" href="https://www.google.com/uds/api/search/1.0/432dd570d1a386253361f581254f9ca1/default+en.css" rel="stylesheet"><link type="text/css" href="https://www.google.com/cse/style/look/v2/default.css" rel="stylesheet"><script type="text/javascript" src="https://www.google.com/uds/api/search/1.0/432dd570d1a386253361f581254f9ca1/default+en.I.js"></script><style type="text/css">
.gsc-control-cse {
font-family: Arial, sans-serif;
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gsc-control-cse .gsc-table-result {
font-family: Arial, sans-serif;
}
input.gsc-input, .gsc-input-box, .gsc-input-box-hover, .gsc-input-box-focus {
border-color: #D9D9D9;
}
input.gsc-search-button, input.gsc-search-button:hover, input.gsc-search-button:focus {
border-color: #2F5BB7;
background-color: #357AE8;
background-image: none;
filter: none;
}
.gsc-tabHeader.gsc-tabhInactive {
border-color: #CCCCCC;
background-color: #FFFFFF;
}
.gsc-tabHeader.gsc-tabhActive {
border-color: #CCCCCC;
border-bottom-color: #FFFFFF;
background-color: #FFFFFF;
}
.gsc-tabsArea {
border-color: #CCCCCC;
}
.gsc-webResult.gsc-result,
.gsc-results .gsc-imageResult {
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gsc-webResult.gsc-result:hover,
.gsc-imageResult:hover {
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gs-webResult.gs-result a.gs-title:link,
.gs-webResult.gs-result a.gs-title:link b,
.gs-imageResult a.gs-title:link,
.gs-imageResult a.gs-title:link b {
color: #1155CC;
}
.gs-webResult.gs-result a.gs-title:visited,
.gs-webResult.gs-result a.gs-title:visited b,
.gs-imageResult a.gs-title:visited,
.gs-imageResult a.gs-title:visited b {
color: #1155CC;
}
.gs-webResult.gs-result a.gs-title:hover,
.gs-webResult.gs-result a.gs-title:hover b,
.gs-imageResult a.gs-title:hover,
.gs-imageResult a.gs-title:hover b {
color: #1155CC;
}
.gs-webResult.gs-result a.gs-title:active,
.gs-webResult.gs-result a.gs-title:active b,
.gs-imageResult a.gs-title:active,
.gs-imageResult a.gs-title:active b {
color: #1155CC;
}
.gsc-cursor-page {
color: #1155CC;
}
a.gsc-trailing-more-results:link {
color: #1155CC;
}
.gs-webResult .gs-snippet,
.gs-imageResult .gs-snippet,
.gs-fileFormatType {
color: #333333;
}
.gs-webResult div.gs-visibleUrl,
.gs-imageResult div.gs-visibleUrl {
color: #009933;
}
.gs-webResult div.gs-visibleUrl-short {
color: #009933;
}
.gs-webResult div.gs-visibleUrl-short {
display: none;
}
.gs-webResult div.gs-visibleUrl-long {
display: block;
}
.gs-promotion div.gs-visibleUrl-short {
display: none;
}
.gs-promotion div.gs-visibleUrl-long {
display: block;
}
.gsc-cursor-box {
border-color: #FFFFFF;
}
.gsc-results .gsc-cursor-box .gsc-cursor-page {
border-color: #CCCCCC;
background-color: #FFFFFF;
color: #1155CC;
}
.gsc-results .gsc-cursor-box .gsc-cursor-current-page {
border-color: #CCCCCC;
background-color: #FFFFFF;
color: #1155CC;
}
.gsc-webResult.gsc-result.gsc-promotion {
border-color: #F6F6F6;
background-color: #F6F6F6;
}
.gsc-completion-title {
color: #1155CC;
}
.gsc-completion-snippet {
color: #333333;
}
.gs-promotion a.gs-title:link,
.gs-promotion a.gs-title:link *,
.gs-promotion .gs-snippet a:link {
color: #1155CC;
}
.gs-promotion a.gs-title:visited,
.gs-promotion a.gs-title:visited *,
.gs-promotion .gs-snippet a:visited {
color: #1155CC;
}
.gs-promotion a.gs-title:hover,
.gs-promotion a.gs-title:hover *,
.gs-promotion .gs-snippet a:hover {
color: #1155CC;
}
.gs-promotion a.gs-title:active,
.gs-promotion a.gs-title:active *,
.gs-promotion .gs-snippet a:active {
color: #1155CC;
}
.gs-promotion .gs-snippet,
.gs-promotion .gs-title .gs-promotion-title-right,
.gs-promotion .gs-title .gs-promotion-title-right * {
color: #333333;
}
.gs-promotion .gs-visibleUrl,
.gs-promotion .gs-visibleUrl-short {
color: #009933;
}</style><style type="text/css">.gscb_a{display:inline-block;font:27px/13px arial,sans-serif}.gsst_a .gscb_a{color:#a1b9ed;cursor:pointer}.gsst_a:hover .gscb_a,.gsst_a:focus .gscb_a{color:#36c}.gsst_a{display:inline-block}.gsst_a{cursor:pointer;padding:0 4px}.gsst_a:hover{text-decoration:none!important}.gsst_b{font-size:16px;padding:0 2px;position:relative;user-select:none;-webkit-user-select:none;white-space:nowrap}.gsst_e{opacity:0.55;}.gsst_a:hover .gsst_e,.gsst_a:focus .gsst_e{opacity:0.72;}.gsst_a:active .gsst_e{opacity:1;}.gsst_f{background:white;text-align:left}.gsst_g{background-color:white;border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);margin:-1px -3px;padding:0 6px}.gsst_h{background-color:white;height:1px;margin-bottom:-1px;position:relative;top:-1px}.gsib_a{width:100%;padding:4px 6px 0}.gsib_a,.gsib_b{vertical-align:top}.gssb_c{border:0;position:absolute;z-index:989}.gssb_e{border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);cursor:default}.gssb_f{visibility:hidden;white-space:nowrap}.gssb_k{border:0;display:block;position:absolute;top:0;z-index:988}.gsdd_a{border:none!important}.gsq_a{padding:0}.gscsep_a{display:none}.gssb_a{padding:0 7px}.gssb_a,.gssb_a td{white-space:nowrap;overflow:hidden;line-height:22px}#gssb_b{font-size:11px;color:#36c;text-decoration:none}#gssb_b:hover{font-size:11px;color:#36c;text-decoration:underline}.gssb_g{text-align:center;padding:8px 0 7px;position:relative}.gssb_h{font-size:15px;height:28px;margin:0.2em;-webkit-appearance:button}.gssb_i{background:#eee}.gss_ifl{visibility:hidden;padding-left:5px}.gssb_i .gss_ifl{visibility:visible}a.gssb_j{font-size:13px;color:#36c;text-decoration:none;line-height:100%}a.gssb_j:hover{text-decoration:underline}.gssb_l{height:1px;background-color:#e5e5e5}.gssb_m{color:#000;background:#fff}.gsfe_a{border:1px solid #b9b9b9;border-top-color:#a0a0a0;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);}.gsfe_b{border:1px solid #4d90fe;outline:none;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);}.gssb_a{padding:0 9px}.gsib_a{padding-right:8px;padding-left:8px}.gsst_a{padding-top:3px}.gssb_e{border:0}.gssb_l{margin:5px 0}.gssb_c .gsc-completion-container{position:static}.gssb_c{z-index:5000}.gsc-completion-container table{background:transparent;font-size:inherit;font-family:inherit}.gssb_c > tbody > tr,.gssb_c > tbody > tr > td,.gssb_d,.gssb_d > tbody > tr,.gssb_d > tbody > tr > td,.gssb_e,.gssb_e > tbody > tr,.gssb_e > tbody > tr > td{padding:0;margin:0;border:0}.gssb_a table,.gssb_a table tr,.gssb_a table tr td{padding:0;margin:0;border:0}</style><style type="text/css">span[data-cmchighlight=''] {padding:0px!important;margin:0px!important;display:inline!important;float:none!important;position:static!important;-webkit-print-color-adjust:exact!important}</style><style type="text/css">.cmcredaction {color:#D3D3D3!important;background:#D3D3D3!important;-webkit-user-select:none!important;-khtml-user-select:none !important;-moz-user-select:none!important;-ms-user-select:none!important;-o-user-select:none!important;user-select:none!important;-webkit-print-color-adjust:exact!important}</style></head>
<body style="zoom: 1;">
<div class="w3-topnav w3-card-2 w3-slim topnav" id="topnav" style="position: relative;">
<div class="w3-row w3-light-grey" id="belowtopnav" style="padding-top: 0px;">
<div class="w3-rest">
<div class="w3-row w3-white">
<div class="w3-col l10 m12" id="main">
<h1>HTML <span class="color_h1">Color Picker</span></h1>
<hr>
<div class="w3-row">

  <div class="w3-col l4 m12" style="text-align:center;">
    <h3>Pick a Color:</h3>
    <div style="margin:auto;width:236px;">
      <img style="margin-right:2px;" src="w3/colormap.gif" usemap="#colormap" alt="colormap"><map id="colormap" name="colormap" onmouseout="mouseOutMap()"><area style="cursor:pointer" shape="poly" coords="63,0,72,4,72,15,63,19,54,15,54,4" onclick="clickColor(&quot;#003366&quot;,-200,54)" onmouseover="mouseOverColor(&quot;#003366&quot;)" alt="#003366"><area style="cursor:pointer" shape="poly" coords="81,0,90,4,90,15,81,19,72,15,72,4" onclick="clickColor(&quot;#336699&quot;,-200,72)" onmouseover="mouseOverColor(&quot;#336699&quot;)" alt="#336699"><area style="cursor:pointer" shape="poly" coords="99,0,108,4,108,15,99,19,90,15,90,4" onclick="clickColor(&quot;#3366CC&quot;,-200,90)" onmouseover="mouseOverColor(&quot;#3366CC&quot;)" alt="#3366CC"><area style="cursor:pointer" shape="poly" coords="117,0,126,4,126,15,117,19,108,15,108,4" onclick="clickColor(&quot;#003399&quot;,-200,108)" onmouseover="mouseOverColor(&quot;#003399&quot;)" alt="#003399"><area style="cursor:pointer" shape="poly" coords="135,0,144,4,144,15,135,19,126,15,126,4" onclick="clickColor(&quot;#000099&quot;,-200,126)" onmouseover="mouseOverColor(&quot;#000099&quot;)" alt="#000099"><area style="cursor:pointer" shape="poly" coords="153,0,162,4,162,15,153,19,144,15,144,4" onclick="clickColor(&quot;#0000CC&quot;,-200,144)" onmouseover="mouseOverColor(&quot;#0000CC&quot;)" alt="#0000CC"><area style="cursor:pointer" shape="poly" coords="171,0,180,4,180,15,171,19,162,15,162,4" onclick="clickColor(&quot;#000066&quot;,-200,162)" onmouseover="mouseOverColor(&quot;#000066&quot;)" alt="#000066"><area style="cursor:pointer" shape="poly" coords="54,15,63,19,63,30,54,34,45,30,45,19" onclick="clickColor(&quot;#006666&quot;,-185,45)" onmouseover="mouseOverColor(&quot;#006666&quot;)" alt="#006666"><area style="cursor:pointer" shape="poly" coords="72,15,81,19,81,30,72,34,63,30,63,19" onclick="clickColor(&quot;#006699&quot;,-185,63)" onmouseover="mouseOverColor(&quot;#006699&quot;)" alt="#006699"><area style="cursor:pointer" shape="poly" coords="90,15,99,19,99,30,90,34,81,30,81,19" onclick="clickColor(&quot;#0099CC&quot;,-185,81)" onmouseover="mouseOverColor(&quot;#0099CC&quot;)" alt="#0099CC"><area style="cursor:pointer" shape="poly" coords="108,15,117,19,117,30,108,34,99,30,99,19" onclick="clickColor(&quot;#0066CC&quot;,-185,99)" onmouseover="mouseOverColor(&quot;#0066CC&quot;)" alt="#0066CC"><area style="cursor:pointer" shape="poly" coords="126,15,135,19,135,30,126,34,117,30,117,19" onclick="clickColor(&quot;#0033CC&quot;,-185,117)" onmouseover="mouseOverColor(&quot;#0033CC&quot;)" alt="#0033CC"><area style="cursor:pointer" shape="poly" coords="144,15,153,19,153,30,144,34,135,30,135,19" onclick="clickColor(&quot;#0000FF&quot;,-185,135)" onmouseover="mouseOverColor(&quot;#0000FF&quot;)" alt="#0000FF"><area style="cursor:pointer" shape="poly" coords="162,15,171,19,171,30,162,34,153,30,153,19" onclick="clickColor(&quot;#3333FF&quot;,-185,153)" onmouseover="mouseOverColor(&quot;#3333FF&quot;)" alt="#3333FF"><area style="cursor:pointer" shape="poly" coords="180,15,189,19,189,30,180,34,171,30,171,19" onclick="clickColor(&quot;#333399&quot;,-185,171)" onmouseover="mouseOverColor(&quot;#333399&quot;)" alt="#333399"><area style="cursor:pointer" shape="poly" coords="45,30,54,34,54,45,45,49,36,45,36,34" onclick="clickColor(&quot;#669999&quot;,-170,36)" onmouseover="mouseOverColor(&quot;#669999&quot;)" alt="#669999"><area style="cursor:pointer" shape="poly" coords="63,30,72,34,72,45,63,49,54,45,54,34" onclick="clickColor(&quot;#009999&quot;,-170,54)" onmouseover="mouseOverColor(&quot;#009999&quot;)" alt="#009999"><area style="cursor:pointer" shape="poly" coords="81,30,90,34,90,45,81,49,72,45,72,34" onclick="clickColor(&quot;#33CCCC&quot;,-170,72)" onmouseover="mouseOverColor(&quot;#33CCCC&quot;)" alt="#33CCCC"><area style="cursor:pointer" shape="poly" coords="99,30,108,34,108,45,99,49,90,45,90,34" onclick="clickColor(&quot;#00CCFF&quot;,-170,90)" onmouseover="mouseOverColor(&quot;#00CCFF&quot;)" alt="#00CCFF"><area style="cursor:pointer" shape="poly" coords="117,30,126,34,126,45,117,49,108,45,108,34" onclick="clickColor(&quot;#0099FF&quot;,-170,108)" onmouseover="mouseOverColor(&quot;#0099FF&quot;)" alt="#0099FF"><area style="cursor:pointer" shape="poly" coords="135,30,144,34,144,45,135,49,126,45,126,34" onclick="clickColor(&quot;#0066FF&quot;,-170,126)" onmouseover="mouseOverColor(&quot;#0066FF&quot;)" alt="#0066FF"><area style="cursor:pointer" shape="poly" coords="153,30,162,34,162,45,153,49,144,45,144,34" onclick="clickColor(&quot;#3366FF&quot;,-170,144)" onmouseover="mouseOverColor(&quot;#3366FF&quot;)" alt="#3366FF"><area style="cursor:pointer" shape="poly" coords="171,30,180,34,180,45,171,49,162,45,162,34" onclick="clickColor(&quot;#3333CC&quot;,-170,162)" onmouseover="mouseOverColor(&quot;#3333CC&quot;)" alt="#3333CC"><area style="cursor:pointer" shape="poly" coords="189,30,198,34,198,45,189,49,180,45,180,34" onclick="clickColor(&quot;#666699&quot;,-170,180)" onmouseover="mouseOverColor(&quot;#666699&quot;)" alt="#666699"><area style="cursor:pointer" shape="poly" coords="36,45,45,49,45,60,36,64,27,60,27,49" onclick="clickColor(&quot;#339966&quot;,-155,27)" onmouseover="mouseOverColor(&quot;#339966&quot;)" alt="#339966"><area style="cursor:pointer" shape="poly" coords="54,45,63,49,63,60,54,64,45,60,45,49" onclick="clickColor(&quot;#00CC99&quot;,-155,45)" onmouseover="mouseOverColor(&quot;#00CC99&quot;)" alt="#00CC99"><area style="cursor:pointer" shape="poly" coords="72,45,81,49,81,60,72,64,63,60,63,49" onclick="clickColor(&quot;#00FFCC&quot;,-155,63)" onmouseover="mouseOverColor(&quot;#00FFCC&quot;)" alt="#00FFCC"><area style="cursor:pointer" shape="poly" coords="90,45,99,49,99,60,90,64,81,60,81,49" onclick="clickColor(&quot;#00FFFF&quot;,-155,81)" onmouseover="mouseOverColor(&quot;#00FFFF&quot;)" alt="#00FFFF"><area style="cursor:pointer" shape="poly" coords="108,45,117,49,117,60,108,64,99,60,99,49" onclick="clickColor(&quot;#33CCFF&quot;,-155,99)" onmouseover="mouseOverColor(&quot;#33CCFF&quot;)" alt="#33CCFF"><area style="cursor:pointer" shape="poly" coords="126,45,135,49,135,60,126,64,117,60,117,49" onclick="clickColor(&quot;#3399FF&quot;,-155,117)" onmouseover="mouseOverColor(&quot;#3399FF&quot;)" alt="#3399FF"><area style="cursor:pointer" shape="poly" coords="144,45,153,49,153,60,144,64,135,60,135,49" onclick="clickColor(&quot;#6699FF&quot;,-155,135)" onmouseover="mouseOverColor(&quot;#6699FF&quot;)" alt="#6699FF"><area style="cursor:pointer" shape="poly" coords="162,45,171,49,171,60,162,64,153,60,153,49" onclick="clickColor(&quot;#6666FF&quot;,-155,153)" onmouseover="mouseOverColor(&quot;#6666FF&quot;)" alt="#6666FF"><area style="cursor:pointer" shape="poly" coords="180,45,189,49,189,60,180,64,171,60,171,49" onclick="clickColor(&quot;#6600FF&quot;,-155,171)" onmouseover="mouseOverColor(&quot;#6600FF&quot;)" alt="#6600FF"><area style="cursor:pointer" shape="poly" coords="198,45,207,49,207,60,198,64,189,60,189,49" onclick="clickColor(&quot;#6600CC&quot;,-155,189)" onmouseover="mouseOverColor(&quot;#6600CC&quot;)" alt="#6600CC"><area style="cursor:pointer" shape="poly" coords="27,60,36,64,36,75,27,79,18,75,18,64" onclick="clickColor(&quot;#339933&quot;,-140,18)" onmouseover="mouseOverColor(&quot;#339933&quot;)" alt="#339933"><area style="cursor:pointer" shape="poly" coords="45,60,54,64,54,75,45,79,36,75,36,64" onclick="clickColor(&quot;#00CC66&quot;,-140,36)" onmouseover="mouseOverColor(&quot;#00CC66&quot;)" alt="#00CC66"><area style="cursor:pointer" shape="poly" coords="63,60,72,64,72,75,63,79,54,75,54,64" onclick="clickColor(&quot;#00FF99&quot;,-140,54)" onmouseover="mouseOverColor(&quot;#00FF99&quot;)" alt="#00FF99"><area style="cursor:pointer" shape="poly" coords="81,60,90,64,90,75,81,79,72,75,72,64" onclick="clickColor(&quot;#66FFCC&quot;,-140,72)" onmouseover="mouseOverColor(&quot;#66FFCC&quot;)" alt="#66FFCC"><area style="cursor:pointer" shape="poly" coords="99,60,108,64,108,75,99,79,90,75,90,64" onclick="clickColor(&quot;#66FFFF&quot;,-140,90)" onmouseover="mouseOverColor(&quot;#66FFFF&quot;)" alt="#66FFFF"><area style="cursor:pointer" shape="poly" coords="117,60,126,64,126,75,117,79,108,75,108,64" onclick="clickColor(&quot;#66CCFF&quot;,-140,108)" onmouseover="mouseOverColor(&quot;#66CCFF&quot;)" alt="#66CCFF"><area style="cursor:pointer" shape="poly" coords="135,60,144,64,144,75,135,79,126,75,126,64" onclick="clickColor(&quot;#99CCFF&quot;,-140,126)" onmouseover="mouseOverColor(&quot;#99CCFF&quot;)" alt="#99CCFF"><area style="cursor:pointer" shape="poly" coords="153,60,162,64,162,75,153,79,144,75,144,64" onclick="clickColor(&quot;#9999FF&quot;,-140,144)" onmouseover="mouseOverColor(&quot;#9999FF&quot;)" alt="#9999FF"><area style="cursor:pointer" shape="poly" coords="171,60,180,64,180,75,171,79,162,75,162,64" onclick="clickColor(&quot;#9966FF&quot;,-140,162)" onmouseover="mouseOverColor(&quot;#9966FF&quot;)" alt="#9966FF"><area style="cursor:pointer" shape="poly" coords="189,60,198,64,198,75,189,79,180,75,180,64" onclick="clickColor(&quot;#9933FF&quot;,-140,180)" onmouseover="mouseOverColor(&quot;#9933FF&quot;)" alt="#9933FF"><area style="cursor:pointer" shape="poly" coords="207,60,216,64,216,75,207,79,198,75,198,64" onclick="clickColor(&quot;#9900FF&quot;,-140,198)" onmouseover="mouseOverColor(&quot;#9900FF&quot;)" alt="#9900FF"><area style="cursor:pointer" shape="poly" coords="18,75,27,79,27,90,18,94,9,90,9,79" onclick="clickColor(&quot;#006600&quot;,-125,9)" onmouseover="mouseOverColor(&quot;#006600&quot;)" alt="#006600"><area style="cursor:pointer" shape="poly" coords="36,75,45,79,45,90,36,94,27,90,27,79" onclick="clickColor(&quot;#00CC00&quot;,-125,27)" onmouseover="mouseOverColor(&quot;#00CC00&quot;)" alt="#00CC00"><area style="cursor:pointer" shape="poly" coords="54,75,63,79,63,90,54,94,45,90,45,79" onclick="clickColor(&quot;#00FF00&quot;,-125,45)" onmouseover="mouseOverColor(&quot;#00FF00&quot;)" alt="#00FF00"><area style="cursor:pointer" shape="poly" coords="72,75,81,79,81,90,72,94,63,90,63,79" onclick="clickColor(&quot;#66FF99&quot;,-125,63)" onmouseover="mouseOverColor(&quot;#66FF99&quot;)" alt="#66FF99"><area style="cursor:pointer" shape="poly" coords="90,75,99,79,99,90,90,94,81,90,81,79" onclick="clickColor(&quot;#99FFCC&quot;,-125,81)" onmouseover="mouseOverColor(&quot;#99FFCC&quot;)" alt="#99FFCC"><area style="cursor:pointer" shape="poly" coords="108,75,117,79,117,90,108,94,99,90,99,79" onclick="clickColor(&quot;#CCFFFF&quot;,-125,99)" onmouseover="mouseOverColor(&quot;#CCFFFF&quot;)" alt="#CCFFFF"><area style="cursor:pointer" shape="poly" coords="126,75,135,79,135,90,126,94,117,90,117,79" onclick="clickColor(&quot;#CCCCFF&quot;,-125,117)" onmouseover="mouseOverColor(&quot;#CCCCFF&quot;)" alt="#CCCCFF"><area style="cursor:pointer" shape="poly" coords="144,75,153,79,153,90,144,94,135,90,135,79" onclick="clickColor(&quot;#CC99FF&quot;,-125,135)" onmouseover="mouseOverColor(&quot;#CC99FF&quot;)" alt="#CC99FF"><area style="cursor:pointer" shape="poly" coords="162,75,171,79,171,90,162,94,153,90,153,79" onclick="clickColor(&quot;#CC66FF&quot;,-125,153)" onmouseover="mouseOverColor(&quot;#CC66FF&quot;)" alt="#CC66FF"><area style="cursor:pointer" shape="poly" coords="180,75,189,79,189,90,180,94,171,90,171,79" onclick="clickColor(&quot;#CC33FF&quot;,-125,171)" onmouseover="mouseOverColor(&quot;#CC33FF&quot;)" alt="#CC33FF"><area style="cursor:pointer" shape="poly" coords="198,75,207,79,207,90,198,94,189,90,189,79" onclick="clickColor(&quot;#CC00FF&quot;,-125,189)" onmouseover="mouseOverColor(&quot;#CC00FF&quot;)" alt="#CC00FF"><area style="cursor:pointer" shape="poly" coords="216,75,225,79,225,90,216,94,207,90,207,79" onclick="clickColor(&quot;#9900CC&quot;,-125,207)" onmouseover="mouseOverColor(&quot;#9900CC&quot;)" alt="#9900CC"><area style="cursor:pointer" shape="poly" coords="9,90,18,94,18,105,9,109,0,105,0,94" onclick="clickColor(&quot;#003300&quot;,-110,0)" onmouseover="mouseOverColor(&quot;#003300&quot;)" alt="#003300"><area style="cursor:pointer" shape="poly" coords="27,90,36,94,36,105,27,109,18,105,18,94" onclick="clickColor(&quot;#009933&quot;,-110,18)" onmouseover="mouseOverColor(&quot;#009933&quot;)" alt="#009933"><area style="cursor:pointer" shape="poly" coords="45,90,54,94,54,105,45,109,36,105,36,94" onclick="clickColor(&quot;#33CC33&quot;,-110,36)" onmouseover="mouseOverColor(&quot;#33CC33&quot;)" alt="#33CC33"><area style="cursor:pointer" shape="poly" coords="63,90,72,94,72,105,63,109,54,105,54,94" onclick="clickColor(&quot;#66FF66&quot;,-110,54)" onmouseover="mouseOverColor(&quot;#66FF66&quot;)" alt="#66FF66"><area style="cursor:pointer" shape="poly" coords="81,90,90,94,90,105,81,109,72,105,72,94" onclick="clickColor(&quot;#99FF99&quot;,-110,72)" onmouseover="mouseOverColor(&quot;#99FF99&quot;)" alt="#99FF99"><area style="cursor:pointer" shape="poly" coords="99,90,108,94,108,105,99,109,90,105,90,94" onclick="clickColor(&quot;#CCFFCC&quot;,-110,90)" onmouseover="mouseOverColor(&quot;#CCFFCC&quot;)" alt="#CCFFCC"><area style="cursor:pointer" shape="poly" coords="117,90,126,94,126,105,117,109,108,105,108,94" onclick="clickColor(&quot;#FFFFFF&quot;,-110,108)" onmouseover="mouseOverColor(&quot;#FFFFFF&quot;)" alt="#FFFFFF"><area style="cursor:pointer" shape="poly" coords="135,90,144,94,144,105,135,109,126,105,126,94" onclick="clickColor(&quot;#FFCCFF&quot;,-110,126)" onmouseover="mouseOverColor(&quot;#FFCCFF&quot;)" alt="#FFCCFF"><area style="cursor:pointer" shape="poly" coords="153,90,162,94,162,105,153,109,144,105,144,94" onclick="clickColor(&quot;#FF99FF&quot;,-110,144)" onmouseover="mouseOverColor(&quot;#FF99FF&quot;)" alt="#FF99FF"><area style="cursor:pointer" shape="poly" coords="171,90,180,94,180,105,171,109,162,105,162,94" onclick="clickColor(&quot;#FF66FF&quot;,-110,162)" onmouseover="mouseOverColor(&quot;#FF66FF&quot;)" alt="#FF66FF"><area style="cursor:pointer" shape="poly" coords="189,90,198,94,198,105,189,109,180,105,180,94" onclick="clickColor(&quot;#FF00FF&quot;,-110,180)" onmouseover="mouseOverColor(&quot;#FF00FF&quot;)" alt="#FF00FF"><area style="cursor:pointer" shape="poly" coords="207,90,216,94,216,105,207,109,198,105,198,94" onclick="clickColor(&quot;#CC00CC&quot;,-110,198)" onmouseover="mouseOverColor(&quot;#CC00CC&quot;)" alt="#CC00CC"><area style="cursor:pointer" shape="poly" coords="225,90,234,94,234,105,225,109,216,105,216,94" onclick="clickColor(&quot;#660066&quot;,-110,216)" onmouseover="mouseOverColor(&quot;#660066&quot;)" alt="#660066"><area style="cursor:pointer" shape="poly" coords="18,105,27,109,27,120,18,124,9,120,9,109" onclick="clickColor(&quot;#336600&quot;,-95,9)" onmouseover="mouseOverColor(&quot;#336600&quot;)" alt="#336600"><area style="cursor:pointer" shape="poly" coords="36,105,45,109,45,120,36,124,27,120,27,109" onclick="clickColor(&quot;#009900&quot;,-95,27)" onmouseover="mouseOverColor(&quot;#009900&quot;)" alt="#009900"><area style="cursor:pointer" shape="poly" coords="54,105,63,109,63,120,54,124,45,120,45,109" onclick="clickColor(&quot;#66FF33&quot;,-95,45)" onmouseover="mouseOverColor(&quot;#66FF33&quot;)" alt="#66FF33"><area style="cursor:pointer" shape="poly" coords="72,105,81,109,81,120,72,124,63,120,63,109" onclick="clickColor(&quot;#99FF66&quot;,-95,63)" onmouseover="mouseOverColor(&quot;#99FF66&quot;)" alt="#99FF66"><area style="cursor:pointer" shape="poly" coords="90,105,99,109,99,120,90,124,81,120,81,109" onclick="clickColor(&quot;#CCFF99&quot;,-95,81)" onmouseover="mouseOverColor(&quot;#CCFF99&quot;)" alt="#CCFF99"><area style="cursor:pointer" shape="poly" coords="108,105,117,109,117,120,108,124,99,120,99,109" onclick="clickColor(&quot;#FFFFCC&quot;,-95,99)" onmouseover="mouseOverColor(&quot;#FFFFCC&quot;)" alt="#FFFFCC"><area style="cursor:pointer" shape="poly" coords="126,105,135,109,135,120,126,124,117,120,117,109" onclick="clickColor(&quot;#FFCCCC&quot;,-95,117)" onmouseover="mouseOverColor(&quot;#FFCCCC&quot;)" alt="#FFCCCC"><area style="cursor:pointer" shape="poly" coords="144,105,153,109,153,120,144,124,135,120,135,109" onclick="clickColor(&quot;#FF99CC&quot;,-95,135)" onmouseover="mouseOverColor(&quot;#FF99CC&quot;)" alt="#FF99CC"><area style="cursor:pointer" shape="poly" coords="162,105,171,109,171,120,162,124,153,120,153,109" onclick="clickColor(&quot;#FF66CC&quot;,-95,153)" onmouseover="mouseOverColor(&quot;#FF66CC&quot;)" alt="#FF66CC"><area style="cursor:pointer" shape="poly" coords="180,105,189,109,189,120,180,124,171,120,171,109" onclick="clickColor(&quot;#FF33CC&quot;,-95,171)" onmouseover="mouseOverColor(&quot;#FF33CC&quot;)" alt="#FF33CC"><area style="cursor:pointer" shape="poly" coords="198,105,207,109,207,120,198,124,189,120,189,109" onclick="clickColor(&quot;#CC0099&quot;,-95,189)" onmouseover="mouseOverColor(&quot;#CC0099&quot;)" alt="#CC0099"><area style="cursor:pointer" shape="poly" coords="216,105,225,109,225,120,216,124,207,120,207,109" onclick="clickColor(&quot;#993399&quot;,-95,207)" onmouseover="mouseOverColor(&quot;#993399&quot;)" alt="#993399"><area style="cursor:pointer" shape="poly" coords="27,120,36,124,36,135,27,139,18,135,18,124" onclick="clickColor(&quot;#333300&quot;,-80,18)" onmouseover="mouseOverColor(&quot;#333300&quot;)" alt="#333300"><area style="cursor:pointer" shape="poly" coords="45,120,54,124,54,135,45,139,36,135,36,124" onclick="clickColor(&quot;#669900&quot;,-80,36)" onmouseover="mouseOverColor(&quot;#669900&quot;)" alt="#669900"><area style="cursor:pointer" shape="poly" coords="63,120,72,124,72,135,63,139,54,135,54,124" onclick="clickColor(&quot;#99FF33&quot;,-80,54)" onmouseover="mouseOverColor(&quot;#99FF33&quot;)" alt="#99FF33"><area style="cursor:pointer" shape="poly" coords="81,120,90,124,90,135,81,139,72,135,72,124" onclick="clickColor(&quot;#CCFF66&quot;,-80,72)" onmouseover="mouseOverColor(&quot;#CCFF66&quot;)" alt="#CCFF66"><area style="cursor:pointer" shape="poly" coords="99,120,108,124,108,135,99,139,90,135,90,124" onclick="clickColor(&quot;#FFFF99&quot;,-80,90)" onmouseover="mouseOverColor(&quot;#FFFF99&quot;)" alt="#FFFF99"><area style="cursor:pointer" shape="poly" coords="117,120,126,124,126,135,117,139,108,135,108,124" onclick="clickColor(&quot;#FFCC99&quot;,-80,108)" onmouseover="mouseOverColor(&quot;#FFCC99&quot;)" alt="#FFCC99"><area style="cursor:pointer" shape="poly" coords="135,120,144,124,144,135,135,139,126,135,126,124" onclick="clickColor(&quot;#FF9999&quot;,-80,126)" onmouseover="mouseOverColor(&quot;#FF9999&quot;)" alt="#FF9999"><area style="cursor:pointer" shape="poly" coords="153,120,162,124,162,135,153,139,144,135,144,124" onclick="clickColor(&quot;#FF6699&quot;,-80,144)" onmouseover="mouseOverColor(&quot;#FF6699&quot;)" alt="#FF6699"><area style="cursor:pointer" shape="poly" coords="171,120,180,124,180,135,171,139,162,135,162,124" onclick="clickColor(&quot;#FF3399&quot;,-80,162)" onmouseover="mouseOverColor(&quot;#FF3399&quot;)" alt="#FF3399"><area style="cursor:pointer" shape="poly" coords="189,120,198,124,198,135,189,139,180,135,180,124" onclick="clickColor(&quot;#CC3399&quot;,-80,180)" onmouseover="mouseOverColor(&quot;#CC3399&quot;)" alt="#CC3399"><area style="cursor:pointer" shape="poly" coords="207,120,216,124,216,135,207,139,198,135,198,124" onclick="clickColor(&quot;#990099&quot;,-80,198)" onmouseover="mouseOverColor(&quot;#990099&quot;)" alt="#990099"><area style="cursor:pointer" shape="poly" coords="36,135,45,139,45,150,36,154,27,150,27,139" onclick="clickColor(&quot;#666633&quot;,-65,27)" onmouseover="mouseOverColor(&quot;#666633&quot;)" alt="#666633"><area style="cursor:pointer" shape="poly" coords="54,135,63,139,63,150,54,154,45,150,45,139" onclick="clickColor(&quot;#99CC00&quot;,-65,45)" onmouseover="mouseOverColor(&quot;#99CC00&quot;)" alt="#99CC00"><area style="cursor:pointer" shape="poly" coords="72,135,81,139,81,150,72,154,63,150,63,139" onclick="clickColor(&quot;#CCFF33&quot;,-65,63)" onmouseover="mouseOverColor(&quot;#CCFF33&quot;)" alt="#CCFF33"><area style="cursor:pointer" shape="poly" coords="90,135,99,139,99,150,90,154,81,150,81,139" onclick="clickColor(&quot;#FFFF66&quot;,-65,81)" onmouseover="mouseOverColor(&quot;#FFFF66&quot;)" alt="#FFFF66"><area style="cursor:pointer" shape="poly" coords="108,135,117,139,117,150,108,154,99,150,99,139" onclick="clickColor(&quot;#FFCC66&quot;,-65,99)" onmouseover="mouseOverColor(&quot;#FFCC66&quot;)" alt="#FFCC66"><area style="cursor:pointer" shape="poly" coords="126,135,135,139,135,150,126,154,117,150,117,139" onclick="clickColor(&quot;#FF9966&quot;,-65,117)" onmouseover="mouseOverColor(&quot;#FF9966&quot;)" alt="#FF9966"><area style="cursor:pointer" shape="poly" coords="144,135,153,139,153,150,144,154,135,150,135,139" onclick="clickColor(&quot;#FF6666&quot;,-65,135)" onmouseover="mouseOverColor(&quot;#FF6666&quot;)" alt="#FF6666"><area style="cursor:pointer" shape="poly" coords="162,135,171,139,171,150,162,154,153,150,153,139" onclick="clickColor(&quot;#FF0066&quot;,-65,153)" onmouseover="mouseOverColor(&quot;#FF0066&quot;)" alt="#FF0066"><area style="cursor:pointer" shape="poly" coords="180,135,189,139,189,150,180,154,171,150,171,139" onclick="clickColor(&quot;#CC6699&quot;,-65,171)" onmouseover="mouseOverColor(&quot;#CC6699&quot;)" alt="#CC6699"><area style="cursor:pointer" shape="poly" coords="198,135,207,139,207,150,198,154,189,150,189,139" onclick="clickColor(&quot;#993366&quot;,-65,189)" onmouseover="mouseOverColor(&quot;#993366&quot;)" alt="#993366"><area style="cursor:pointer" shape="poly" coords="45,150,54,154,54,165,45,169,36,165,36,154" onclick="clickColor(&quot;#999966&quot;,-50,36)" onmouseover="mouseOverColor(&quot;#999966&quot;)" alt="#999966"><area style="cursor:pointer" shape="poly" coords="63,150,72,154,72,165,63,169,54,165,54,154" onclick="clickColor(&quot;#CCCC00&quot;,-50,54)" onmouseover="mouseOverColor(&quot;#CCCC00&quot;)" alt="#CCCC00"><area style="cursor:pointer" shape="poly" coords="81,150,90,154,90,165,81,169,72,165,72,154" onclick="clickColor(&quot;#FFFF00&quot;,-50,72)" onmouseover="mouseOverColor(&quot;#FFFF00&quot;)" alt="#FFFF00"><area style="cursor:pointer" shape="poly" coords="99,150,108,154,108,165,99,169,90,165,90,154" onclick="clickColor(&quot;#FFCC00&quot;,-50,90)" onmouseover="mouseOverColor(&quot;#FFCC00&quot;)" alt="#FFCC00"><area style="cursor:pointer" shape="poly" coords="117,150,126,154,126,165,117,169,108,165,108,154" onclick="clickColor(&quot;#FF9933&quot;,-50,108)" onmouseover="mouseOverColor(&quot;#FF9933&quot;)" alt="#FF9933"><area style="cursor:pointer" shape="poly" coords="135,150,144,154,144,165,135,169,126,165,126,154" onclick="clickColor(&quot;#FF6600&quot;,-50,126)" onmouseover="mouseOverColor(&quot;#FF6600&quot;)" alt="#FF6600"><area style="cursor:pointer" shape="poly" coords="153,150,162,154,162,165,153,169,144,165,144,154" onclick="clickColor(&quot;#FF5050&quot;,-50,144)" onmouseover="mouseOverColor(&quot;#FF5050&quot;)" alt="#FF5050"><area style="cursor:pointer" shape="poly" coords="171,150,180,154,180,165,171,169,162,165,162,154" onclick="clickColor(&quot;#CC0066&quot;,-50,162)" onmouseover="mouseOverColor(&quot;#CC0066&quot;)" alt="#CC0066"><area style="cursor:pointer" shape="poly" coords="189,150,198,154,198,165,189,169,180,165,180,154" onclick="clickColor(&quot;#660033&quot;,-50,180)" onmouseover="mouseOverColor(&quot;#660033&quot;)" alt="#660033"><area style="cursor:pointer" shape="poly" coords="54,165,63,169,63,180,54,184,45,180,45,169" onclick="clickColor(&quot;#996633&quot;,-35,45)" onmouseover="mouseOverColor(&quot;#996633&quot;)" alt="#996633"><area style="cursor:pointer" shape="poly" coords="72,165,81,169,81,180,72,184,63,180,63,169" onclick="clickColor(&quot;#CC9900&quot;,-35,63)" onmouseover="mouseOverColor(&quot;#CC9900&quot;)" alt="#CC9900"><area style="cursor:pointer" shape="poly" coords="90,165,99,169,99,180,90,184,81,180,81,169" onclick="clickColor(&quot;#FF9900&quot;,-35,81)" onmouseover="mouseOverColor(&quot;#FF9900&quot;)" alt="#FF9900"><area style="cursor:pointer" shape="poly" coords="108,165,117,169,117,180,108,184,99,180,99,169" onclick="clickColor(&quot;#CC6600&quot;,-35,99)" onmouseover="mouseOverColor(&quot;#CC6600&quot;)" alt="#CC6600"><area style="cursor:pointer" shape="poly" coords="126,165,135,169,135,180,126,184,117,180,117,169" onclick="clickColor(&quot;#FF3300&quot;,-35,117)" onmouseover="mouseOverColor(&quot;#FF3300&quot;)" alt="#FF3300"><area style="cursor:pointer" shape="poly" coords="144,165,153,169,153,180,144,184,135,180,135,169" onclick="clickColor(&quot;#FF0000&quot;,-35,135)" onmouseover="mouseOverColor(&quot;#FF0000&quot;)" alt="#FF0000"><area style="cursor:pointer" shape="poly" coords="162,165,171,169,171,180,162,184,153,180,153,169" onclick="clickColor(&quot;#CC0000&quot;,-35,153)" onmouseover="mouseOverColor(&quot;#CC0000&quot;)" alt="#CC0000"><area style="cursor:pointer" shape="poly" coords="180,165,189,169,189,180,180,184,171,180,171,169" onclick="clickColor(&quot;#990033&quot;,-35,171)" onmouseover="mouseOverColor(&quot;#990033&quot;)" alt="#990033"><area style="cursor:pointer" shape="poly" coords="63,180,72,184,72,195,63,199,54,195,54,184" onclick="clickColor(&quot;#663300&quot;,-20,54)" onmouseover="mouseOverColor(&quot;#663300&quot;)" alt="#663300"><area style="cursor:pointer" shape="poly" coords="81,180,90,184,90,195,81,199,72,195,72,184" onclick="clickColor(&quot;#996600&quot;,-20,72)" onmouseover="mouseOverColor(&quot;#996600&quot;)" alt="#996600"><area style="cursor:pointer" shape="poly" coords="99,180,108,184,108,195,99,199,90,195,90,184" onclick="clickColor(&quot;#CC3300&quot;,-20,90)" onmouseover="mouseOverColor(&quot;#CC3300&quot;)" alt="#CC3300"><area style="cursor:pointer" shape="poly" coords="117,180,126,184,126,195,117,199,108,195,108,184" onclick="clickColor(&quot;#993300&quot;,-20,108)" onmouseover="mouseOverColor(&quot;#993300&quot;)" alt="#993300"><area style="cursor:pointer" shape="poly" coords="135,180,144,184,144,195,135,199,126,195,126,184" onclick="clickColor(&quot;#990000&quot;,-20,126)" onmouseover="mouseOverColor(&quot;#990000&quot;)" alt="#990000"><area style="cursor:pointer" shape="poly" coords="153,180,162,184,162,195,153,199,144,195,144,184" onclick="clickColor(&quot;#800000&quot;,-20,144)" onmouseover="mouseOverColor(&quot;#800000&quot;)" alt="#800000"><area style="cursor:pointer" shape="poly" coords="171,180,180,184,180,195,171,199,162,195,162,184" onclick="clickColor(&quot;#993333&quot;,-20,162)" onmouseover="mouseOverColor(&quot;#993333&quot;)" alt="#993333"></map>
<script>
var thistop = "";
var thisleft = "";	
</script>

      <div id="selectedhexagon" style="visibility: visible; position: relative; width: 21px; height: 21px; top: -35px; left: 135px; background-image: url(&quot;selectedcolor.gif&quot;);"></div>
    <div id="divpreview">&nbsp;</div>
      <h3>Or Enter a Color:</h3>
      <div id="entercolorDIV" class="">
        <input type="text" id="entercolor" placeholder="Colorvalue" onfocus="clearWrongInput();" style="z-index:0;"><button class="btn btn-default" type="button" onclick="clickColor(0,-1,-1)" style="z-index:0;">OK</button>
      </div>
      <div id="wronginputDIV" style="display: none;">Wrong Input</div>
      <br>
      <div id="html5DIV">
        <h3>Or Use HTML5:</h3>
        <input type="color" id="html5colorpicker" onchange="clickColor(0, -1, -1, 5)" value="#ff0000" style="width:85%;">
      </div>
      <br>
      <br>        
    </div>
  </div>

  <div class="w3-col l4 m12" style="text-align:center;">
    <h3>Selected Color:</h3>
    <div id="selectedcolor" class="w3-large" style="background-color: rgb(255, 0, 0);">
    <br><br>
    <span class="w3-text-black">Black Text</span><br><br>
    <span class="w3-text-black w3-text-shadow">Shadow</span><br><br>
    <span class="w3-text-white">White Text</span><br><br>
    <span class="w3-text-white w3-text-shadow">Shadow</span>
    </div>
    <div id="colornamDIV" class="w3-margin-top">red</div>      
    <div><b><span id="colorhexDIV">#ff0000</span></b></div>
    <div id="colorrgbDIV">rgb(255, 0, 0)</div>
    <div id="colorhslDIV">hsl(0, 100%, 50%)</div>    
    <div id="colorhsvDIV">hsv(0, 100%, 100%)</div>        
  </div>

  <div class="w3-col l4 m12">
<!--<div id="huetopcontainer"></div>-->
    <div id="lumtopcontainer"><h3 class="w3-center">Lighter / Darker:</h3><table class="colorTable" style="width:100%;"><tbody><tr><td style="width:40px;text-align:right;">100%&nbsp;</td><td style="cursor:pointer;background-color:#ffffff" onclick="clickColor(&quot;#ffffff&quot;)"></td><td style="width:80px;">&nbsp;#ffffff</td></tr><tr><td style="width:40px;text-align:right;">95%&nbsp;</td><td style="cursor:pointer;background-color:#ffe5e5" onclick="clickColor(&quot;#ffe5e5&quot;)"></td><td style="width:80px;">&nbsp;#ffe5e5</td></tr><tr><td style="width:40px;text-align:right;">90%&nbsp;</td><td style="cursor:pointer;background-color:#ffcccc" onclick="clickColor(&quot;#ffcccc&quot;)"></td><td style="width:80px;">&nbsp;#ffcccc</td></tr><tr><td style="width:40px;text-align:right;">85%&nbsp;</td><td style="cursor:pointer;background-color:#ffb3b3" onclick="clickColor(&quot;#ffb3b3&quot;)"></td><td style="width:80px;">&nbsp;#ffb3b3</td></tr><tr><td style="width:40px;text-align:right;">80%&nbsp;</td><td style="cursor:pointer;background-color:#ff9999" onclick="clickColor(&quot;#ff9999&quot;)"></td><td style="width:80px;">&nbsp;#ff9999</td></tr><tr><td style="width:40px;text-align:right;">75%&nbsp;</td><td style="cursor:pointer;background-color:#ff8080" onclick="clickColor(&quot;#ff8080&quot;)"></td><td style="width:80px;">&nbsp;#ff8080</td></tr><tr><td style="width:40px;text-align:right;">70%&nbsp;</td><td style="cursor:pointer;background-color:#ff6666" onclick="clickColor(&quot;#ff6666&quot;)"></td><td style="width:80px;">&nbsp;#ff6666</td></tr><tr><td style="width:40px;text-align:right;">65%&nbsp;</td><td style="cursor:pointer;background-color:#ff4d4d" onclick="clickColor(&quot;#ff4d4d&quot;)"></td><td style="width:80px;">&nbsp;#ff4d4d</td></tr><tr><td style="width:40px;text-align:right;">60%&nbsp;</td><td style="cursor:pointer;background-color:#ff3333" onclick="clickColor(&quot;#ff3333&quot;)"></td><td style="width:80px;">&nbsp;#ff3333</td></tr><tr><td style="width:40px;text-align:right;">55%&nbsp;</td><td style="cursor:pointer;background-color:#ff1a1a" onclick="clickColor(&quot;#ff1a1a&quot;)"></td><td style="width:80px;">&nbsp;#ff1a1a</td></tr><tr><td></td><td></td><td></td></tr><tr><td style="text-align:right;"><b>50%&nbsp;</b></td><td style="background-color:#ff0000"><br><br></td><td>&nbsp;<b>#ff0000</b></td></tr><tr><td></td><td></td><td></td></tr><tr><td style="width:40px;text-align:right;">45%&nbsp;</td><td style="cursor:pointer;background-color:#e60000" onclick="clickColor(&quot;#e60000&quot;)"></td><td style="width:80px;">&nbsp;#e60000</td></tr><tr><td style="width:40px;text-align:right;">40%&nbsp;</td><td style="cursor:pointer;background-color:#cc0000" onclick="clickColor(&quot;#cc0000&quot;)"></td><td style="width:80px;">&nbsp;#cc0000</td></tr><tr><td style="width:40px;text-align:right;">35%&nbsp;</td><td style="cursor:pointer;background-color:#b30000" onclick="clickColor(&quot;#b30000&quot;)"></td><td style="width:80px;">&nbsp;#b30000</td></tr><tr><td style="width:40px;text-align:right;">30%&nbsp;</td><td style="cursor:pointer;background-color:#990000" onclick="clickColor(&quot;#990000&quot;)"></td><td style="width:80px;">&nbsp;#990000</td></tr><tr><td style="width:40px;text-align:right;">25%&nbsp;</td><td style="cursor:pointer;background-color:#800000" onclick="clickColor(&quot;#800000&quot;)"></td><td style="width:80px;">&nbsp;#800000</td></tr><tr><td style="width:40px;text-align:right;">20%&nbsp;</td><td style="cursor:pointer;background-color:#660000" onclick="clickColor(&quot;#660000&quot;)"></td><td style="width:80px;">&nbsp;#660000</td></tr><tr><td style="width:40px;text-align:right;">15%&nbsp;</td><td style="cursor:pointer;background-color:#4d0000" onclick="clickColor(&quot;#4d0000&quot;)"></td><td style="width:80px;">&nbsp;#4d0000</td></tr><tr><td style="width:40px;text-align:right;">10%&nbsp;</td><td style="cursor:pointer;background-color:#330000" onclick="clickColor(&quot;#330000&quot;)"></td><td style="width:80px;">&nbsp;#330000</td></tr><tr><td style="width:40px;text-align:right;">5%&nbsp;</td><td style="cursor:pointer;background-color:#1a0000" onclick="clickColor(&quot;#1a0000&quot;)"></td><td style="width:80px;">&nbsp;#1a0000</td></tr><tr><td style="width:40px;text-align:right;">0%&nbsp;</td><td style="cursor:pointer;background-color:#000000" onclick="clickColor(&quot;#000000&quot;)"></td><td style="width:80px;">&nbsp;#000000</td></tr></tbody></table></div>    
  </div>


</div>

<hr>
<div id="huecontainer"><h3>Hue</h3><div class="table-responsive"><table class="w3-table-all colorTable" style="width:100%;white-space: nowrap;font-size:14px;"><tbody><tr><td style="width:150px;"></td><td style="text-align:right;">Hue&nbsp;</td><td>Hex</td><td>Rgb</td><td>Hsl</td><td>Hsv</td></tr><tr><td style="background-color:#ff0000"></td><td style="text-align:right;"><b>0&nbsp;</b></td><td><b>#ff0000</b></td><td><b>rgb(255, 0, 0)</b></td><td><b>hsl(0, 100%, 50%)</b></td><td><b>hsv(0, 100%, 100%)</b></td></tr><tr><td style="cursor:pointer;background-color:#ff4000" onclick="clickColor(&quot;#ff4000&quot;)"></td><td style="text-align:right;">15&nbsp;</td><td>#ff4000</td><td>rgb(255, 64, 0)</td><td>hsl(15, 100%, 50%)</td><td>hsv(15, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff8000" onclick="clickColor(&quot;#ff8000&quot;)"></td><td style="text-align:right;">30&nbsp;</td><td>#ff8000</td><td>rgb(255, 128, 0)</td><td>hsl(30, 100%, 50%)</td><td>hsv(30, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffbf00" onclick="clickColor(&quot;#ffbf00&quot;)"></td><td style="text-align:right;">45&nbsp;</td><td>#ffbf00</td><td>rgb(255, 191, 0)</td><td>hsl(45, 100%, 50%)</td><td>hsv(45, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffff00" onclick="clickColor(&quot;#ffff00&quot;)"></td><td style="text-align:right;">60&nbsp;</td><td>#ffff00</td><td>rgb(255, 255, 0)</td><td>hsl(60, 100%, 50%)</td><td>hsv(60, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#bfff00" onclick="clickColor(&quot;#bfff00&quot;)"></td><td style="text-align:right;">75&nbsp;</td><td>#bfff00</td><td>rgb(191, 255, 0)</td><td>hsl(75, 100%, 50%)</td><td>hsv(75, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#80ff00" onclick="clickColor(&quot;#80ff00&quot;)"></td><td style="text-align:right;">90&nbsp;</td><td>#80ff00</td><td>rgb(128, 255, 0)</td><td>hsl(90, 100%, 50%)</td><td>hsv(90, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#40ff00" onclick="clickColor(&quot;#40ff00&quot;)"></td><td style="text-align:right;">105&nbsp;</td><td>#40ff00</td><td>rgb(64, 255, 0)</td><td>hsl(105, 100%, 50%)</td><td>hsv(105, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#00ff00" onclick="clickColor(&quot;#00ff00&quot;)"></td><td style="text-align:right;">120&nbsp;</td><td>#00ff00</td><td>rgb(0, 255, 0)</td><td>hsl(120, 100%, 50%)</td><td>hsv(120, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#00ff40" onclick="clickColor(&quot;#00ff40&quot;)"></td><td style="text-align:right;">135&nbsp;</td><td>#00ff40</td><td>rgb(0, 255, 64)</td><td>hsl(135, 100%, 50%)</td><td>hsv(135, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#00ff80" onclick="clickColor(&quot;#00ff80&quot;)"></td><td style="text-align:right;">150&nbsp;</td><td>#00ff80</td><td>rgb(0, 255, 128)</td><td>hsl(150, 100%, 50%)</td><td>hsv(150, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#00ffbf" onclick="clickColor(&quot;#00ffbf&quot;)"></td><td style="text-align:right;">165&nbsp;</td><td>#00ffbf</td><td>rgb(0, 255, 191)</td><td>hsl(165, 100%, 50%)</td><td>hsv(165, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#00ffff" onclick="clickColor(&quot;#00ffff&quot;)"></td><td style="text-align:right;">180&nbsp;</td><td>#00ffff</td><td>rgb(0, 255, 255)</td><td>hsl(180, 100%, 50%)</td><td>hsv(180, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#00bfff" onclick="clickColor(&quot;#00bfff&quot;)"></td><td style="text-align:right;">195&nbsp;</td><td>#00bfff</td><td>rgb(0, 191, 255)</td><td>hsl(195, 100%, 50%)</td><td>hsv(195, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#007fff" onclick="clickColor(&quot;#007fff&quot;)"></td><td style="text-align:right;">210&nbsp;</td><td>#007fff</td><td>rgb(0, 127, 255)</td><td>hsl(210, 100%, 50%)</td><td>hsv(210, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#0040ff" onclick="clickColor(&quot;#0040ff&quot;)"></td><td style="text-align:right;">225&nbsp;</td><td>#0040ff</td><td>rgb(0, 64, 255)</td><td>hsl(225, 100%, 50%)</td><td>hsv(225, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#0000ff" onclick="clickColor(&quot;#0000ff&quot;)"></td><td style="text-align:right;">240&nbsp;</td><td>#0000ff</td><td>rgb(0, 0, 255)</td><td>hsl(240, 100%, 50%)</td><td>hsv(240, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#4000ff" onclick="clickColor(&quot;#4000ff&quot;)"></td><td style="text-align:right;">255&nbsp;</td><td>#4000ff</td><td>rgb(64, 0, 255)</td><td>hsl(255, 100%, 50%)</td><td>hsv(255, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#7f00ff" onclick="clickColor(&quot;#7f00ff&quot;)"></td><td style="text-align:right;">270&nbsp;</td><td>#7f00ff</td><td>rgb(127, 0, 255)</td><td>hsl(270, 100%, 50%)</td><td>hsv(270, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#bf00ff" onclick="clickColor(&quot;#bf00ff&quot;)"></td><td style="text-align:right;">285&nbsp;</td><td>#bf00ff</td><td>rgb(191, 0, 255)</td><td>hsl(285, 100%, 50%)</td><td>hsv(285, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff00ff" onclick="clickColor(&quot;#ff00ff&quot;)"></td><td style="text-align:right;">300&nbsp;</td><td>#ff00ff</td><td>rgb(255, 0, 255)</td><td>hsl(300, 100%, 50%)</td><td>hsv(300, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff00bf" onclick="clickColor(&quot;#ff00bf&quot;)"></td><td style="text-align:right;">315&nbsp;</td><td>#ff00bf</td><td>rgb(255, 0, 191)</td><td>hsl(315, 100%, 50%)</td><td>hsv(315, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff0080" onclick="clickColor(&quot;#ff0080&quot;)"></td><td style="text-align:right;">330&nbsp;</td><td>#ff0080</td><td>rgb(255, 0, 128)</td><td>hsl(330, 100%, 50%)</td><td>hsv(330, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff0040" onclick="clickColor(&quot;#ff0040&quot;)"></td><td style="text-align:right;">345&nbsp;</td><td>#ff0040</td><td>rgb(255, 0, 64)</td><td>hsl(345, 100%, 50%)</td><td>hsv(345, 100%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff0000" onclick="clickColor(&quot;#ff0000&quot;)"></td><td style="text-align:right;">360&nbsp;</td><td>#ff0000</td><td>rgb(255, 0, 0)</td><td>hsl(0, 100%, 50%)</td><td>hsv(0, 100%, 100%)</td></tr></tbody></table></div></div>
<hr>
<div id="hslsatcontainer"><h3>HSL Saturation</h3><div class="table-responsive"><table class="w3-table-all colorTable" style="width:100%;white-space: nowrap;font-size:14px;"><tbody><tr><td style="width:150px;"></td><td style="text-align:right;">Sat&nbsp;</td><td>Hex</td><td>Rgb</td><td>Hsl</td><td>Hsv</td></tr><tr><td style="background-color:#ff0000"></td><td style="text-align:right;"><b>100%&nbsp;</b></td><td><b>#ff0000</b></td><td><b>rgb(255, 0, 0)</b></td><td><b>hsl(0, 100%, 50%)</b></td><td><b>hsv(0, 100%, 100%)</b></td></tr><tr><td style="cursor:pointer;background-color:#f90606" onclick="clickColor(&quot;#f90606&quot;)"></td><td style="text-align:right;">95%&nbsp;</td><td>#f90606</td><td>rgb(249, 6, 6)</td><td>hsl(0, 95%, 50%)</td><td>hsv(0, 97%, 98%)</td></tr><tr><td style="cursor:pointer;background-color:#f20d0d" onclick="clickColor(&quot;#f20d0d&quot;)"></td><td style="text-align:right;">90%&nbsp;</td><td>#f20d0d</td><td>rgb(242, 13, 13)</td><td>hsl(0, 90%, 50%)</td><td>hsv(0, 95%, 95%)</td></tr><tr><td style="cursor:pointer;background-color:#ec1313" onclick="clickColor(&quot;#ec1313&quot;)"></td><td style="text-align:right;">85%&nbsp;</td><td>#ec1313</td><td>rgb(236, 19, 19)</td><td>hsl(0, 85%, 50%)</td><td>hsv(0, 92%, 93%)</td></tr><tr><td style="cursor:pointer;background-color:#e61919" onclick="clickColor(&quot;#e61919&quot;)"></td><td style="text-align:right;">80%&nbsp;</td><td>#e61919</td><td>rgb(230, 25, 25)</td><td>hsl(0, 80%, 50%)</td><td>hsv(0, 89%, 90%)</td></tr><tr><td style="cursor:pointer;background-color:#df2020" onclick="clickColor(&quot;#df2020&quot;)"></td><td style="text-align:right;">75%&nbsp;</td><td>#df2020</td><td>rgb(223, 32, 32)</td><td>hsl(0, 75%, 50%)</td><td>hsv(0, 86%, 88%)</td></tr><tr><td style="cursor:pointer;background-color:#d92626" onclick="clickColor(&quot;#d92626&quot;)"></td><td style="text-align:right;">70%&nbsp;</td><td>#d92626</td><td>rgb(217, 38, 38)</td><td>hsl(0, 70%, 50%)</td><td>hsv(0, 82%, 85%)</td></tr><tr><td style="cursor:pointer;background-color:#d22d2d" onclick="clickColor(&quot;#d22d2d&quot;)"></td><td style="text-align:right;">65%&nbsp;</td><td>#d22d2d</td><td>rgb(210, 45, 45)</td><td>hsl(0, 65%, 50%)</td><td>hsv(0, 79%, 83%)</td></tr><tr><td style="cursor:pointer;background-color:#cc3333" onclick="clickColor(&quot;#cc3333&quot;)"></td><td style="text-align:right;">60%&nbsp;</td><td>#cc3333</td><td>rgb(204, 51, 51)</td><td>hsl(0, 60%, 50%)</td><td>hsv(0, 75%, 80%)</td></tr><tr><td style="cursor:pointer;background-color:#c63939" onclick="clickColor(&quot;#c63939&quot;)"></td><td style="text-align:right;">55%&nbsp;</td><td>#c63939</td><td>rgb(198, 57, 57)</td><td>hsl(0, 55%, 50%)</td><td>hsv(0, 71%, 78%)</td></tr><tr><td style="cursor:pointer;background-color:#bf4040" onclick="clickColor(&quot;#bf4040&quot;)"></td><td style="text-align:right;">50%&nbsp;</td><td>#bf4040</td><td>rgb(191, 64, 64)</td><td>hsl(0, 50%, 50%)</td><td>hsv(0, 67%, 75%)</td></tr><tr><td style="cursor:pointer;background-color:#b94646" onclick="clickColor(&quot;#b94646&quot;)"></td><td style="text-align:right;">45%&nbsp;</td><td>#b94646</td><td>rgb(185, 70, 70)</td><td>hsl(0, 45%, 50%)</td><td>hsv(0, 62%, 73%)</td></tr><tr><td style="cursor:pointer;background-color:#b34d4d" onclick="clickColor(&quot;#b34d4d&quot;)"></td><td style="text-align:right;">40%&nbsp;</td><td>#b34d4d</td><td>rgb(179, 77, 77)</td><td>hsl(0, 40%, 50%)</td><td>hsv(0, 57%, 70%)</td></tr><tr><td style="cursor:pointer;background-color:#ac5353" onclick="clickColor(&quot;#ac5353&quot;)"></td><td style="text-align:right;">35%&nbsp;</td><td>#ac5353</td><td>rgb(172, 83, 83)</td><td>hsl(0, 35%, 50%)</td><td>hsv(0, 52%, 68%)</td></tr><tr><td style="cursor:pointer;background-color:#a65959" onclick="clickColor(&quot;#a65959&quot;)"></td><td style="text-align:right;">30%&nbsp;</td><td>#a65959</td><td>rgb(166, 89, 89)</td><td>hsl(0, 30%, 50%)</td><td>hsv(0, 46%, 65%)</td></tr><tr><td style="cursor:pointer;background-color:#9f6060" onclick="clickColor(&quot;#9f6060&quot;)"></td><td style="text-align:right;">25%&nbsp;</td><td>#9f6060</td><td>rgb(159, 96, 96)</td><td>hsl(0, 25%, 50%)</td><td>hsv(0, 40%, 63%)</td></tr><tr><td style="cursor:pointer;background-color:#996666" onclick="clickColor(&quot;#996666&quot;)"></td><td style="text-align:right;">20%&nbsp;</td><td>#996666</td><td>rgb(153, 102, 102)</td><td>hsl(0, 20%, 50%)</td><td>hsv(0, 33%, 60%)</td></tr><tr><td style="cursor:pointer;background-color:#936c6c" onclick="clickColor(&quot;#936c6c&quot;)"></td><td style="text-align:right;">15%&nbsp;</td><td>#936c6c</td><td>rgb(147, 108, 108)</td><td>hsl(0, 15%, 50%)</td><td>hsv(0, 26%, 58%)</td></tr><tr><td style="cursor:pointer;background-color:#8c7373" onclick="clickColor(&quot;#8c7373&quot;)"></td><td style="text-align:right;">10%&nbsp;</td><td>#8c7373</td><td>rgb(140, 115, 115)</td><td>hsl(0, 10%, 50%)</td><td>hsv(0, 18%, 55%)</td></tr><tr><td style="cursor:pointer;background-color:#867979" onclick="clickColor(&quot;#867979&quot;)"></td><td style="text-align:right;">5%&nbsp;</td><td>#867979</td><td>rgb(134, 121, 121)</td><td>hsl(0, 5%, 50%)</td><td>hsv(0, 10%, 53%)</td></tr><tr><td style="cursor:pointer;background-color:#808080" onclick="clickColor(&quot;#808080&quot;)"></td><td style="text-align:right;">0%&nbsp;</td><td>#808080</td><td>rgb(128, 128, 128)</td><td>hsl(0, 0%, 50%)</td><td>hsv(0, 0%, 50%)</td></tr></tbody></table></div></div>
<hr>
<div id="hsllumcontainer"><h3>HSL Lighter / Darker</h3><div class="table-responsive"><table class="w3-table-all colorTable" style="width:100%;white-space: nowrap;font-size:14px;"><tbody><tr><td style="width:150px;"></td><td style="text-align:right;">Lum&nbsp;</td><td>Hex</td><td>Rgb</td><td>Hsl</td><td>Hsv</td></tr><tr><td style="cursor:pointer;background-color:#ffffff" onclick="clickColor(&quot;#ffffff&quot;)"></td><td style="text-align:right;">100%&nbsp;</td><td>#ffffff</td><td>rgb(255, 255, 255)</td><td>hsl(0, 0%, 100%)</td><td>hsv(0, 0%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffe5e5" onclick="clickColor(&quot;#ffe5e5&quot;)"></td><td style="text-align:right;">95%&nbsp;</td><td>#ffe5e5</td><td>rgb(255, 229, 229)</td><td>hsl(0, 100%, 95%)</td><td>hsv(0, 10%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffcccc" onclick="clickColor(&quot;#ffcccc&quot;)"></td><td style="text-align:right;">90%&nbsp;</td><td>#ffcccc</td><td>rgb(255, 204, 204)</td><td>hsl(0, 100%, 90%)</td><td>hsv(0, 20%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffb3b3" onclick="clickColor(&quot;#ffb3b3&quot;)"></td><td style="text-align:right;">85%&nbsp;</td><td>#ffb3b3</td><td>rgb(255, 179, 179)</td><td>hsl(0, 100%, 85%)</td><td>hsv(0, 30%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff9999" onclick="clickColor(&quot;#ff9999&quot;)"></td><td style="text-align:right;">80%&nbsp;</td><td>#ff9999</td><td>rgb(255, 153, 153)</td><td>hsl(0, 100%, 80%)</td><td>hsv(0, 40%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff8080" onclick="clickColor(&quot;#ff8080&quot;)"></td><td style="text-align:right;">75%&nbsp;</td><td>#ff8080</td><td>rgb(255, 128, 128)</td><td>hsl(0, 100%, 75%)</td><td>hsv(0, 50%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff6666" onclick="clickColor(&quot;#ff6666&quot;)"></td><td style="text-align:right;">70%&nbsp;</td><td>#ff6666</td><td>rgb(255, 102, 102)</td><td>hsl(0, 100%, 70%)</td><td>hsv(0, 60%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff4d4d" onclick="clickColor(&quot;#ff4d4d&quot;)"></td><td style="text-align:right;">65%&nbsp;</td><td>#ff4d4d</td><td>rgb(255, 77, 77)</td><td>hsl(0, 100%, 65%)</td><td>hsv(0, 70%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff3333" onclick="clickColor(&quot;#ff3333&quot;)"></td><td style="text-align:right;">60%&nbsp;</td><td>#ff3333</td><td>rgb(255, 51, 51)</td><td>hsl(0, 100%, 60%)</td><td>hsv(0, 80%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff1a1a" onclick="clickColor(&quot;#ff1a1a&quot;)"></td><td style="text-align:right;">55%&nbsp;</td><td>#ff1a1a</td><td>rgb(255, 26, 26)</td><td>hsl(0, 100%, 55%)</td><td>hsv(0, 90%, 100%)</td></tr><tr><td style="background-color:#ff0000"></td><td style="text-align:right;"><b>50%&nbsp;</b></td><td><b>#ff0000</b></td><td><b>rgb(255, 0, 0)</b></td><td><b>hsl(0, 100%, 50%)</b></td><td><b>hsv(0, 100%, 100%)</b></td></tr><tr><td style="cursor:pointer;background-color:#e60000" onclick="clickColor(&quot;#e60000&quot;)"></td><td style="text-align:right;">45%&nbsp;</td><td>#e60000</td><td>rgb(230, 0, 0)</td><td>hsl(0, 100%, 45%)</td><td>hsv(0, 100%, 90%)</td></tr><tr><td style="cursor:pointer;background-color:#cc0000" onclick="clickColor(&quot;#cc0000&quot;)"></td><td style="text-align:right;">40%&nbsp;</td><td>#cc0000</td><td>rgb(204, 0, 0)</td><td>hsl(0, 100%, 40%)</td><td>hsv(0, 100%, 80%)</td></tr><tr><td style="cursor:pointer;background-color:#b30000" onclick="clickColor(&quot;#b30000&quot;)"></td><td style="text-align:right;">35%&nbsp;</td><td>#b30000</td><td>rgb(179, 0, 0)</td><td>hsl(0, 100%, 35%)</td><td>hsv(0, 100%, 70%)</td></tr><tr><td style="cursor:pointer;background-color:#990000" onclick="clickColor(&quot;#990000&quot;)"></td><td style="text-align:right;">30%&nbsp;</td><td>#990000</td><td>rgb(153, 0, 0)</td><td>hsl(0, 100%, 30%)</td><td>hsv(0, 100%, 60%)</td></tr><tr><td style="cursor:pointer;background-color:#800000" onclick="clickColor(&quot;#800000&quot;)"></td><td style="text-align:right;">25%&nbsp;</td><td>#800000</td><td>rgb(128, 0, 0)</td><td>hsl(0, 100%, 25%)</td><td>hsv(0, 100%, 50%)</td></tr><tr><td style="cursor:pointer;background-color:#660000" onclick="clickColor(&quot;#660000&quot;)"></td><td style="text-align:right;">20%&nbsp;</td><td>#660000</td><td>rgb(102, 0, 0)</td><td>hsl(0, 100%, 20%)</td><td>hsv(0, 100%, 40%)</td></tr><tr><td style="cursor:pointer;background-color:#4d0000" onclick="clickColor(&quot;#4d0000&quot;)"></td><td style="text-align:right;">15%&nbsp;</td><td>#4d0000</td><td>rgb(77, 0, 0)</td><td>hsl(0, 100%, 15%)</td><td>hsv(0, 100%, 30%)</td></tr><tr><td style="cursor:pointer;background-color:#330000" onclick="clickColor(&quot;#330000&quot;)"></td><td style="text-align:right;">10%&nbsp;</td><td>#330000</td><td>rgb(51, 0, 0)</td><td>hsl(0, 100%, 10%)</td><td>hsv(0, 100%, 20%)</td></tr><tr><td style="cursor:pointer;background-color:#1a0000" onclick="clickColor(&quot;#1a0000&quot;)"></td><td style="text-align:right;">5%&nbsp;</td><td>#1a0000</td><td>rgb(26, 0, 0)</td><td>hsl(0, 100%, 5%)</td><td>hsv(0, 100%, 10%)</td></tr><tr><td style="cursor:pointer;background-color:#000000" onclick="clickColor(&quot;#000000&quot;)"></td><td style="text-align:right;">0%&nbsp;</td><td>#000000</td><td>rgb(0, 0, 0)</td><td>hsl(0, 0%, 0%)</td><td>hsv(0, 0%, 0%)</td></tr></tbody></table></div></div>
<hr>
<div id="hsvsatcontainer"><h3>HSV Saturation</h3><div class="table-responsive"><table class="w3-table-all colorTable" style="width:100%;white-space: nowrap;font-size:14px;"><tbody><tr><td style="width:150px;"></td><td style="text-align:right;">Sat&nbsp;</td><td>Hex</td><td>Rgb</td><td>Hsl</td><td>Hsv</td></tr><tr><td style="background-color:#ff0000"></td><td style="text-align:right;"><b>100%&nbsp;</b></td><td><b>#ff0000</b></td><td><b>rgb(255, 0, 0)</b></td><td><b>hsl(0, 100%, 50%)</b></td><td><b>hsv(0, 100%, 100%)</b></td></tr><tr><td style="cursor:pointer;background-color:#ff0d0d" onclick="clickColor(&quot;#ff0d0d&quot;)"></td><td style="text-align:right;">95%&nbsp;</td><td>#ff0d0d</td><td>rgb(255, 13, 13)</td><td>hsl(0, 100%, 53%)</td><td>hsv(0, 95%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff1919" onclick="clickColor(&quot;#ff1919&quot;)"></td><td style="text-align:right;">90%&nbsp;</td><td>#ff1919</td><td>rgb(255, 25, 25)</td><td>hsl(0, 100%, 55%)</td><td>hsv(0, 90%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff2626" onclick="clickColor(&quot;#ff2626&quot;)"></td><td style="text-align:right;">85%&nbsp;</td><td>#ff2626</td><td>rgb(255, 38, 38)</td><td>hsl(0, 100%, 57%)</td><td>hsv(0, 85%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff3333" onclick="clickColor(&quot;#ff3333&quot;)"></td><td style="text-align:right;">80%&nbsp;</td><td>#ff3333</td><td>rgb(255, 51, 51)</td><td>hsl(0, 100%, 60%)</td><td>hsv(0, 80%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff4040" onclick="clickColor(&quot;#ff4040&quot;)"></td><td style="text-align:right;">75%&nbsp;</td><td>#ff4040</td><td>rgb(255, 64, 64)</td><td>hsl(0, 100%, 63%)</td><td>hsv(0, 75%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff4d4d" onclick="clickColor(&quot;#ff4d4d&quot;)"></td><td style="text-align:right;">70%&nbsp;</td><td>#ff4d4d</td><td>rgb(255, 77, 77)</td><td>hsl(0, 100%, 65%)</td><td>hsv(0, 70%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff5959" onclick="clickColor(&quot;#ff5959&quot;)"></td><td style="text-align:right;">65%&nbsp;</td><td>#ff5959</td><td>rgb(255, 89, 89)</td><td>hsl(0, 100%, 68%)</td><td>hsv(0, 65%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff6666" onclick="clickColor(&quot;#ff6666&quot;)"></td><td style="text-align:right;">60%&nbsp;</td><td>#ff6666</td><td>rgb(255, 102, 102)</td><td>hsl(0, 100%, 70%)</td><td>hsv(0, 60%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff7373" onclick="clickColor(&quot;#ff7373&quot;)"></td><td style="text-align:right;">55%&nbsp;</td><td>#ff7373</td><td>rgb(255, 115, 115)</td><td>hsl(0, 100%, 73%)</td><td>hsv(0, 55%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff8080" onclick="clickColor(&quot;#ff8080&quot;)"></td><td style="text-align:right;">50%&nbsp;</td><td>#ff8080</td><td>rgb(255, 128, 128)</td><td>hsl(0, 100%, 75%)</td><td>hsv(0, 50%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff8c8c" onclick="clickColor(&quot;#ff8c8c&quot;)"></td><td style="text-align:right;">45%&nbsp;</td><td>#ff8c8c</td><td>rgb(255, 140, 140)</td><td>hsl(0, 100%, 78%)</td><td>hsv(0, 45%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ff9999" onclick="clickColor(&quot;#ff9999&quot;)"></td><td style="text-align:right;">40%&nbsp;</td><td>#ff9999</td><td>rgb(255, 153, 153)</td><td>hsl(0, 100%, 80%)</td><td>hsv(0, 40%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffa6a6" onclick="clickColor(&quot;#ffa6a6&quot;)"></td><td style="text-align:right;">35%&nbsp;</td><td>#ffa6a6</td><td>rgb(255, 166, 166)</td><td>hsl(0, 100%, 83%)</td><td>hsv(0, 35%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffb3b3" onclick="clickColor(&quot;#ffb3b3&quot;)"></td><td style="text-align:right;">30%&nbsp;</td><td>#ffb3b3</td><td>rgb(255, 179, 179)</td><td>hsl(0, 100%, 85%)</td><td>hsv(0, 30%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffbfbf" onclick="clickColor(&quot;#ffbfbf&quot;)"></td><td style="text-align:right;">25%&nbsp;</td><td>#ffbfbf</td><td>rgb(255, 191, 191)</td><td>hsl(0, 100%, 88%)</td><td>hsv(0, 25%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffcccc" onclick="clickColor(&quot;#ffcccc&quot;)"></td><td style="text-align:right;">20%&nbsp;</td><td>#ffcccc</td><td>rgb(255, 204, 204)</td><td>hsl(0, 100%, 90%)</td><td>hsv(0, 20%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffd9d9" onclick="clickColor(&quot;#ffd9d9&quot;)"></td><td style="text-align:right;">15%&nbsp;</td><td>#ffd9d9</td><td>rgb(255, 217, 217)</td><td>hsl(0, 100%, 93%)</td><td>hsv(0, 15%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffe6e6" onclick="clickColor(&quot;#ffe6e6&quot;)"></td><td style="text-align:right;">10%&nbsp;</td><td>#ffe6e6</td><td>rgb(255, 230, 230)</td><td>hsl(0, 100%, 95%)</td><td>hsv(0, 10%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#fff2f2" onclick="clickColor(&quot;#fff2f2&quot;)"></td><td style="text-align:right;">5%&nbsp;</td><td>#fff2f2</td><td>rgb(255, 242, 242)</td><td>hsl(0, 100%, 98%)</td><td>hsv(0, 5%, 100%)</td></tr><tr><td style="cursor:pointer;background-color:#ffffff" onclick="clickColor(&quot;#ffffff&quot;)"></td><td style="text-align:right;">0%&nbsp;</td><td>#ffffff</td><td>rgb(255, 255, 255)</td><td>hsl(0, 0%, 100%)</td><td>hsv(0, 0%, 100%)</td></tr></tbody></table></div></div>
<hr>
<div id="hsvvalcontainer"><h3>HSV Brighter / Darker</h3><div class="table-responsive"><table class="w3-table-all colorTable" style="width:100%;white-space: nowrap;font-size:14px;"><tbody><tr><td style="width:150px;"></td><td style="text-align:right;">Value&nbsp;</td><td>Hex</td><td>Rgb</td><td>Hsl</td><td>Hsv</td></tr><tr><td style="background-color:#ff0000"></td><td style="text-align:right;"><b>100%&nbsp;</b></td><td><b>#ff0000</b></td><td><b>rgb(255, 0, 0)</b></td><td><b>hsl(0, 100%, 50%)</b></td><td><b>hsv(0, 100%, 100%)</b></td></tr><tr><td style="cursor:pointer;background-color:#f20000" onclick="clickColor(&quot;#f20000&quot;)"></td><td style="text-align:right;">95%&nbsp;</td><td>#f20000</td><td>rgb(242, 0, 0)</td><td>hsl(0, 100%, 48%)</td><td>hsv(0, 100%, 95%)</td></tr><tr><td style="cursor:pointer;background-color:#e60000" onclick="clickColor(&quot;#e60000&quot;)"></td><td style="text-align:right;">90%&nbsp;</td><td>#e60000</td><td>rgb(230, 0, 0)</td><td>hsl(0, 100%, 45%)</td><td>hsv(0, 100%, 90%)</td></tr><tr><td style="cursor:pointer;background-color:#d90000" onclick="clickColor(&quot;#d90000&quot;)"></td><td style="text-align:right;">85%&nbsp;</td><td>#d90000</td><td>rgb(217, 0, 0)</td><td>hsl(0, 100%, 43%)</td><td>hsv(0, 100%, 85%)</td></tr><tr><td style="cursor:pointer;background-color:#cc0000" onclick="clickColor(&quot;#cc0000&quot;)"></td><td style="text-align:right;">80%&nbsp;</td><td>#cc0000</td><td>rgb(204, 0, 0)</td><td>hsl(0, 100%, 40%)</td><td>hsv(0, 100%, 80%)</td></tr><tr><td style="cursor:pointer;background-color:#bf0000" onclick="clickColor(&quot;#bf0000&quot;)"></td><td style="text-align:right;">75%&nbsp;</td><td>#bf0000</td><td>rgb(191, 0, 0)</td><td>hsl(0, 100%, 38%)</td><td>hsv(0, 100%, 75%)</td></tr><tr><td style="cursor:pointer;background-color:#b30000" onclick="clickColor(&quot;#b30000&quot;)"></td><td style="text-align:right;">70%&nbsp;</td><td>#b30000</td><td>rgb(179, 0, 0)</td><td>hsl(0, 100%, 35%)</td><td>hsv(0, 100%, 70%)</td></tr><tr><td style="cursor:pointer;background-color:#a60000" onclick="clickColor(&quot;#a60000&quot;)"></td><td style="text-align:right;">65%&nbsp;</td><td>#a60000</td><td>rgb(166, 0, 0)</td><td>hsl(0, 100%, 33%)</td><td>hsv(0, 100%, 65%)</td></tr><tr><td style="cursor:pointer;background-color:#990000" onclick="clickColor(&quot;#990000&quot;)"></td><td style="text-align:right;">60%&nbsp;</td><td>#990000</td><td>rgb(153, 0, 0)</td><td>hsl(0, 100%, 30%)</td><td>hsv(0, 100%, 60%)</td></tr><tr><td style="cursor:pointer;background-color:#8c0000" onclick="clickColor(&quot;#8c0000&quot;)"></td><td style="text-align:right;">55%&nbsp;</td><td>#8c0000</td><td>rgb(140, 0, 0)</td><td>hsl(0, 100%, 28%)</td><td>hsv(0, 100%, 55%)</td></tr><tr><td style="cursor:pointer;background-color:#800000" onclick="clickColor(&quot;#800000&quot;)"></td><td style="text-align:right;">50%&nbsp;</td><td>#800000</td><td>rgb(128, 0, 0)</td><td>hsl(0, 100%, 25%)</td><td>hsv(0, 100%, 50%)</td></tr><tr><td style="cursor:pointer;background-color:#730000" onclick="clickColor(&quot;#730000&quot;)"></td><td style="text-align:right;">45%&nbsp;</td><td>#730000</td><td>rgb(115, 0, 0)</td><td>hsl(0, 100%, 23%)</td><td>hsv(0, 100%, 45%)</td></tr><tr><td style="cursor:pointer;background-color:#660000" onclick="clickColor(&quot;#660000&quot;)"></td><td style="text-align:right;">40%&nbsp;</td><td>#660000</td><td>rgb(102, 0, 0)</td><td>hsl(0, 100%, 20%)</td><td>hsv(0, 100%, 40%)</td></tr><tr><td style="cursor:pointer;background-color:#590000" onclick="clickColor(&quot;#590000&quot;)"></td><td style="text-align:right;">35%&nbsp;</td><td>#590000</td><td>rgb(89, 0, 0)</td><td>hsl(0, 100%, 18%)</td><td>hsv(0, 100%, 35%)</td></tr><tr><td style="cursor:pointer;background-color:#4d0000" onclick="clickColor(&quot;#4d0000&quot;)"></td><td style="text-align:right;">30%&nbsp;</td><td>#4d0000</td><td>rgb(77, 0, 0)</td><td>hsl(0, 100%, 15%)</td><td>hsv(0, 100%, 30%)</td></tr><tr><td style="cursor:pointer;background-color:#400000" onclick="clickColor(&quot;#400000&quot;)"></td><td style="text-align:right;">25%&nbsp;</td><td>#400000</td><td>rgb(64, 0, 0)</td><td>hsl(0, 100%, 13%)</td><td>hsv(0, 100%, 25%)</td></tr><tr><td style="cursor:pointer;background-color:#330000" onclick="clickColor(&quot;#330000&quot;)"></td><td style="text-align:right;">20%&nbsp;</td><td>#330000</td><td>rgb(51, 0, 0)</td><td>hsl(0, 100%, 10%)</td><td>hsv(0, 100%, 20%)</td></tr><tr><td style="cursor:pointer;background-color:#260000" onclick="clickColor(&quot;#260000&quot;)"></td><td style="text-align:right;">15%&nbsp;</td><td>#260000</td><td>rgb(38, 0, 0)</td><td>hsl(0, 100%, 8%)</td><td>hsv(0, 100%, 15%)</td></tr><tr><td style="cursor:pointer;background-color:#1a0000" onclick="clickColor(&quot;#1a0000&quot;)"></td><td style="text-align:right;">10%&nbsp;</td><td>#1a0000</td><td>rgb(26, 0, 0)</td><td>hsl(0, 100%, 5%)</td><td>hsv(0, 100%, 10%)</td></tr><tr><td style="cursor:pointer;background-color:#0d0000" onclick="clickColor(&quot;#0d0000&quot;)"></td><td style="text-align:right;">5%&nbsp;</td><td>#0d0000</td><td>rgb(13, 0, 0)</td><td>hsl(0, 100%, 3%)</td><td>hsv(0, 100%, 5%)</td></tr><tr><td style="cursor:pointer;background-color:#000000" onclick="clickColor(&quot;#000000&quot;)"></td><td style="text-align:right;">0%&nbsp;</td><td>#000000</td><td>rgb(0, 0, 0)</td><td>hsl(0, 0%, 0%)</td><td>hsv(0, 0%, 0%)</td></tr></tbody></table></div></div>
<hr>
<!--
<div class="w3-row">
  <div class="w3-col l5 m12">
    <div id="saturationcontainer"></div>
  </div>
  <div class="w3-col l2 m12">&nbsp;</div>
  <div class="w3-col l5 m12" style="text-align:center;">   
    <div id="lumencontainer"></div>
  </div>

</div>

<hr>

<div class="w3-row">
  <div class="w3-col l5 m12">
    <div id="hsvsaturationcontainer"></div>
  </div>
  <div class="w3-col l2 m12">&nbsp;</div>
  <div class="w3-col l5 m12" style="text-align:center;">   
    <div id="hsvbrightencontainer"></div>
  </div>

</div>
<hr>
-->
<h2>RGB (Red, Green, Blue)</h2>
<table class="w3-table w3-border" style="width:100%">
<tbody><tr>
<th>Red</th>
<th>Green</th>
<th>Blue</th>
</tr>
<tr>
<td id="valRed">255</td>
<td id="valGreen">0</td>
<td id="valBlue">0</td>
</tr>
<tr>
<td id="red" style="height:50px;background-color:red"></td>
<td id="green" style="background-color:green"></td>
<td id="blue" style="background-color:blue"></td>
</tr>
<tr>
<td>
<input onchange="changeRed(this.value)" type="range" id="slideRed" name="slideRed" min="0" max="255">
</td>
<td>
<input onchange="changeGreen(this.value)" type="range" id="slideGreen" name="slideGreen" min="0" max="255">
</td>
<td>
<input onchange="changeBlue(this.value)" type="range" id="slideBlue" name="slideBlue" min="0" max="255">
</td>
</tr>
</tbody></table>
<div id="change" style="height: 50px; background-color: rgb(255, 0, 0);"></div>
<div id="fixed" style="height: 50px; background-color: rgb(255, 0, 0);"></div>
<div class="w3-center">
<span id="changetxt">rgb(255, 0, 0)</span>&nbsp;&nbsp;<span id="changetxthex">#ff0000</span>
</div>


<br>
</div>
<div class="w3-col l2 m12" id="right">

<div class="sidesection">
<div id="skyscraper">
<div id="div-gpt-ad-1422003450156-5">
<script>
 googletag.cmd.push(function() {
 googletag.display('div-gpt-ad-1422003450156-5');
 });
 </script> 
<div id="google_ads_iframe_/16833175/WideSkyScraper_0__container__" style="border: 0pt none;"><iframe id="google_ads_iframe_/16833175/WideSkyScraper_0" name="google_ads_iframe_/16833175/WideSkyScraper_0" width="160" height="600" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" src="javascript:&quot;<html><body style='background:transparent'></body></html>&quot;" style="border: 0px; vertical-align: bottom;"></iframe></div><iframe id="google_ads_iframe_/16833175/WideSkyScraper_0__hidden__" name="google_ads_iframe_/16833175/WideSkyScraper_0__hidden__" width="0" height="0" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" src="javascript:&quot;<html><body style='background:transparent'></body></html>&quot;" style="border: 0px; vertical-align: bottom; visibility: hidden; display: none;"></iframe></div>
</div>
</div>

<div class="sidesection">
<h3>W3SCHOOLS EXAMS</h3>
<a target="_blank" href="http://www.w3schools.com/cert/default.asp">HTML, CSS, JavaScript, PHP, jQuery, and XML Certifications</a>
</div>

<div class="sidesection">
<h3>COLOR PICKER</h3>
<a href="/tags/ref_colorpicker.asp">
<img src="/images/colorpicker.gif" alt="colorpicker"></a>
</div>

<div class="sidesection">
<h3>SHARE THIS PAGE</h3>
<div class="w3-text-grey sharethis">
<script>
<!--
try{
loc=location.pathname;
if (loc.toUpperCase().indexOf(".ASP")<0) loc=loc+"default.asp";
txt='<a href="http://www.facebook.com/sharer.php?u=http://www.w3schools.com'+loc+'" target="_blank" title="Facebook"><span class="fa fa-facebook-square fa-2x"></span></a>';
txt=txt+'<a href="http://twitter.com/home?status=Currently reading http://www.w3schools.com'+loc+'" target="_blank" title="Twitter"><span class="fa fa-twitter-square fa-2x"></span></a>';
txt=txt+'<a href="https://plus.google.com/share?url=http://www.w3schools.com'+loc+'" target="_blank" title="Google+"><span class="fa fa-google-plus-square fa-2x"></span></a>';
document.write(txt);
} catch(e) {}
//-->
</script><a href="http://www.facebook.com/sharer.php?u=http://www.w3schools.com/tags/ref_colorpicker.asp" target="_blank" title="Facebook"><span class="fa fa-facebook-square fa-2x"></span></a><a href="http://twitter.com/home?status=Currently reading http://www.w3schools.com/tags/ref_colorpicker.asp" target="_blank" title="Twitter"><span class="fa fa-twitter-square fa-2x"></span></a><a href="https://plus.google.com/share?url=http://www.w3schools.com/tags/ref_colorpicker.asp" target="_blank" title="Google+"><span class="fa fa-google-plus-square fa-2x"></span></a>
</div>
</div>

<div class="sidesection w3-text-grey sharethis">
<a href="javascript:void(0);" onclick="clickFBLike()" title="Like W3Schools on Facebook">
<span class="fa fa-thumbs-o-up fa-2x"></span></a>
<div id="fblikeframe">
<div id="popupframe"></div>
<div id="popupDIV"></div>
</div>
</div>       

<div class="sidesection">
<h3>LEARN MORE:</h3>
<p><a href="/howto/howto_google_maps.asp">Google Maps</a><br>
<a href="/howto/howto_css_animate_buttons.asp">Animated Buttons</a><br>
<a href="/howto/howto_css_modals.asp">Modal Boxes</a><br>
<a href="/howto/howto_js_animate.asp">JS Animations</a><br>
<a href="/howto/howto_js_progressbar.asp">Progress Bars</a><br>
<a href="/howto/howto_js_dropdown.asp">Dropdowns</a><br>
<a href="/howto/howto_html_include.asp">HTML Includes</a><br>
<a href="/w3css/w3css_palettes.asp">Color Palettes</a>
</p>
</div>

</div>
</div>
<script src="/lib/w3schools_footer.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>  
<![endif]-->


<script>
if ("FF0000" == "FF0000") {
    clickColor("FF0000", -35, 135);
} else {
    clickColor("FF0000", -1, -1);
}
if (thistop != "") {
    document.getElementById("selectedhexagon").style.top = thistop + "px";
    document.getElementById("selectedhexagon").style.left = thisleft + "px";
    document.getElementById("selectedhexagon").style.visibility = "visible";
}
</script>
<table cellspacing="0" cellpadding="0" class="gstl_50 gssb_c" style="width: 2px; display: none; top: 3px; left: -1px; position: absolute;"><tbody><tr><td class="gssb_f"></td><td class="gssb_e" style="width: 100%;"></td></tr></tbody></table><div class="cmcMultiHighlighterOverlay" id="cmcMultiHighlighterOverlay1" style="z-index: 1000000"><div class="cmcMultiHighlighterContentWrapper" id="cmcMultiHighlighterContentWrapper1"><div class="cmcMultiHighlighterOverlayContentWrapper cmcMultiHighlighterOverlayContentWrapperLeft" id="cmcMultiHighlighterOverlayContentWrapper1"><span class="cmcMultiHighlighterSearchInputBoxWrapper" id="cmcMultiHighlighterSearchInputBoxWrapper1"><input type="text" class="cmcMultiHighlighterBoxBase cmcMultiHighlighterSearchInputBox" id="cmcMultiHighlighterSearchInputBox1" data-prevval=""></span></div><div class="cmcMultiHighlighterOverlayContentWrapper cmcMultiHighlighterOverlayContentWrapperRight" id="cmcMultiHighlighterOverlayContentWrapper2"><span class="cmcMultiHighlighterBoxBase cmcMultiHighlighterSearchCounts" id="cmcMultiHighlighterSearchCounts1"></span></div><div class="cmcMultiHighlighterOverlayContentWrapper cmcMultiHighlighterOverlayContentWrapperRight" id="cmcMultiHighlighterOverlayContentWrapper3" style="white-space: nowrap;"><span class="cmcmh cmcMultiHighlighterOverlayButtonContainer ui-buttonset" id="cmcMultiHighlighterOverlayButtonContainer1"><input type="checkbox" class="cmcmh ui-helper-hidden-accessible" id="button1RedactText" disabled=""><label class="cmcmh ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right ui-button-disabled ui-state-disabled" for="button1RedactText" role="button" aria-disabled="true"><span class="ui-button-text">R</span></label></span></div><div class="cmcMultiHighlighterOverlayContentWrapper closeWrapper"><a class="cmcMultiHighlighterOverlayClose"></a></div></div></div></body></html>