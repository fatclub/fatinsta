<link href="/css/administrace.css" rel="stylesheet" type="text/css"/>
<script src="/js/jquery-2.1.4.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<body>
    <section class="funkce">
        <ul>
            <li><a href="?action=logout">&#x2715; Odhlásit se</a></li>
            <li><a href="/">Zpět na úvod</a></li>
            <li><a href="?action=showHeader">Upravit hlavičku</a></li>
            <li><a href="?action=listPages">Upravit stránky</a></li>
            <li><a href="?action=listUpdates">Upravit aktuality</a></li>
            <li><a href="?action=addPage">Přidat stránku</a></li>
            <li><a href="?action=addUpdate">Přidat aktualitu</a></li>
        </ul>
    </section>
    <div class="sideClick"><<</div>
</body>