<main>
    <section  class="container">
        <div class="classic_wrap_container">
            <h2 class="content_title">
                <?= $this->title ?>
            </h2>
            <div class="inner_container">
            <?= $this->html ?>                
            </div>
        </div>
        <?php
        if($this->showSideMenu==1){
        require "sideMenu.php"; 
        }
        ?>
    </section>
</main>