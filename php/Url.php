<?php

class Url {

    function returnback() {
        if (isset($_SERVER["HTTP_REFFERER"])) {
            header("location:" . $_SERVER["HTTP_REFFERER"]);
        } else {
            header("location:/administrace/");
        }
    }

    function getClass($value) {
        if ($value["address"] == "/") {
            return "home";
        } else {
            return explode("/", $value["address"])[1];
        }
    }

    function getSideClass($value, $level) {
        $tmp = explode("/", $value["address"]);
        $class = $tmp[count($tmp) - 2];
        if ($level == 0) {
            $class.=" sideMenuL";
        }
        return $class;
    }

    function getSpanClass($level) {
        if ($level === 1) {
            return "sub_sub";
        } else {
            return "";
        }
    }
    function searchchecklength($string, $length, $value,$typ) {
    if (strlen($string) > $length) {
        switch($typ){
            case "aktualita":
//                return mb_substr($string,0,$length,"utf-8")."...<br><a href='/aktuality/?id=" . $value["id"] . "'>Čtěte více.</a>";
                $s = substr($string, 0,$length+1);
   return substr($s, 0, strrpos($s, ' '))."...<br><a class='readmore' href='/aktuality.php?id=" . $value["id"] . "'>Čtěte více.</a>";

            case "page":
//                return mb_substr($string,0,$length)."...<br><a href='/page.php?id=" . $value["id"] . "'>Čtěte více.</a>";
                $s = substr($string, 0,$length+1);
   return substr($s, 0, strrpos($s, ' '))."...<br><a class='readmore' href='".$value["address"]."'>Čtěte více.</a>";
                
        }
        
    } else {
        return $string;
    }
}
function adminchecklength($string, $length, $value){
    if (strlen($string) > $length) {
        $s = substr($string, 0,$length+1);
   return substr($s, 0, strrpos($s, ' '))."...";
    }else{
        return $string;
    }
}

}
