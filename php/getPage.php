<?php
require 'dbsettings.php';
try {
    $pdo = new PDO($dsn, $username, $passwd);
    $getPage = $pdo->prepare("SELECT * FROM pages where id = ?");
    $getPage->execute(array($page_id));
    $Page = $getPage->fetchAll();
} catch (PDOException $ex) {
    echo $ex;
}
?>
<div class="wrap_container sub_page">
    <div class="col-lg-12">
        <h2 class="content_title">
            <?php if($page_id !== 1){echo $Page[0]["title"]; }?></h2>
        <div class="inner_container">
            <?php echo $Page[0]["html"]; ?>
        </div>
    </div>
</div>

