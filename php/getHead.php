<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/js.js" type="text/javascript"></script>
<link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
 <link href="/css/css.css" rel="stylesheet" type="text/css"/>
 <link href='https://fonts.googleapis.com/css?family=Open+Sans:800' rel='stylesheet' type='text/css'/>
<title>
    <?php
    require 'siteSets.php';
    if (isset($this->title)): ?>
        <?= $this->title . $siteSets["suffix"]; ?>
    <?php endif; ?>
</title>
<meta property="og:desctiption" content="<?=  $this->description; ?>">
<meta name="twitter:desctiption" content="<?=  $this->description; ?>">
<meta name="twitter:url" content="<?= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI "]; ?>">
<meta property="og:image" content="" />
<meta property="og:url" content="<?= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI "]; ?>" />
<meta property="og:type" content="page"/>
<meta property="og:title" content="When Great Minds Don’t Think Alike" />