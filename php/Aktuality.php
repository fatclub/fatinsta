<?php 
class Aktuality{
    public function displaySpecific($numb){
        $aktualita = DBM::dotaz("SELECT * FROM news WHERE id=?",array($numb));
        require 'aktuality/showAktualita.php';
    }
    public function displayMain(){
        $aktualita = DBM::dotaz("SELECT * FROM news ORDER BY position LIMIT 3 ");
        require 'aktuality/show3Aktuality.php';
    }
    public function displayAll(){
        $aktualita = DBM::dotaz("SELECT * FROM news ORDER BY position");
        require 'aktuality/showAll.php';
    }
}