<?php
class Administrace{
    public function logout(){
//        session_start();
        session_destroy();
        header("location:/");
    }
    public function login(){
        require "administrace/login.php";
    }
    public function listPages(){
$pages = DBM::dotaz("SELECT id,html,title,url FROM pages");
        require 'administrace/listPages.php';
    }
    public function listUpdates(){
        $pole = DBM::dotaz("SELECT * FROM news ORDER BY position");
        require 'administrace/listUpdates.php';
    }
    public function showHeader(){
        $pole = DBM::dotaz("SELECT pages.id,header.title AS htitle, pages.title,pages.url,header.position FROM header JOIN pages ON(pages.id=header.page_id) ORDER BY position");
        require 'administrace/showHeader.php';
    }
    public function addUpdate(){
        require 'administrace/addUpdate.php';
    }
    public function addPage(){
        require 'administrace/addPage.php';
    }
    public function editPage(){
        $pole = DBM::dotaz("SELECT * FROM pages WHERE id = ?",array($_GET["id"]));
        require 'administrace/editPage.php';
    }
    public function editHeader(){
        
    }
    public function editUpdate($id){
        $aktualita = DBM::dotaz("SELECT * FROM news WHERE id = ?",array($_GET["id"]));
        require 'administrace/editUpdate.php';
    }
    public function createPage(){
        require 'administrace/createPage.php';
    }
    public function insertUpdate(){
        
    }
    public function deletePage($id){
        
    }
    public function deleteUpdate($id){
        
    }
    public function editPageData(){
        require "administrace/editPageData.php";
    }
}

