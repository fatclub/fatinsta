<?php
session_start();
require 'php/startup.php';
DBM::pripoj();
if ($_SERVER["REQUEST_URI"][strlen($_SERVER["REQUEST_URI"]) - 1] != "/" && empty($_GET)) {
    header("location:" . $_SERVER["REQUEST_URI"] . "/");
}
$page = new Page();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php $page->head(); ?>
    </head>
    <body>
        <?php
        $page->header();
        $page->mainContent();
        $page->sideButton();
        $page->footer();
        ?>
    </body>
</html>
